//
//  CategoryViewController.swift
//  EveryReminder
//
//  Created by deneme on 14.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

protocol CategoryViewDelegate: AnyObject {
    func setUI(category: BaseCategory)
}

protocol CategoryTableViewCellDelegate {
    func setAlarmLabel(alarmLabel: String)
}

class CategoryViewController: UIViewController, CategoryViewDelegate {
    
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textFieldContainerView: UIView!
    @IBOutlet var keyboardButtonView: UIView!
    var placeholderLabel: UILabel?
    
    private let categoryPresenter = CategoryPresenter(categoryService: CategoryService())
    weak var categoryViewDelegate: CategoryViewDelegate?
    var baseCategory: BaseCategory?
    var alarmLabel: String?
    var userId: String?
    var alarms: NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryViewDelegate = self
        categoryPresenter.setViewDelegate(categoryViewDelegate: categoryViewDelegate.self)
        setNavigationBar()
        categoryPresenter.getCategories()
        setTextView()
        textView.inputAccessoryView = keyboardButtonView
    }
    
    func setTextView() {
        textView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel?.text = "Enter some text..."
        placeholderLabel?.font = UIFont.italicSystemFont(ofSize: (textView.font?.pointSize)!)
        placeholderLabel?.sizeToFit()
        textView.addSubview(placeholderLabel ?? UILabel())
        placeholderLabel?.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel?.textColor = UIColor.lightGray
        placeholderLabel?.isHidden = !textView.text.isEmpty
        
        textFieldContainerView.layer.cornerRadius = 10
        textFieldContainerView.layer.masksToBounds = true
        textFieldContainerView.layer.borderColor = UIColor.gray.cgColor
        textFieldContainerView.layer.borderWidth = 0.5
        textFieldContainerView.layer.masksToBounds = false
        textFieldContainerView.clipsToBounds = false
    }
    
    func setNavigationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(CategoryViewController.backButtonTapped))
        navigationItem.leftBarButtonItem?.tintColor = .white
        navigationItem.leftBarButtonItem?.isEnabled = true
    }
    
    @objc func backButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func continueBtnTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "toAlarmVC", sender: nil)
    }
    
    func setUI(category: BaseCategory) {
        self.baseCategory = category
        categoryTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let alarmVC = segue.destination as? AlarmViewController, segue.identifier == "toAlarmVC" {
            alarmVC.selectedAlarmLabel = self.alarmLabel
            alarmVC.userId = self.userId
            alarmVC.alarms = self.alarms
        }
    }
}

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return baseCategory?.categories?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTableViewCell", for: indexPath) as! CategoryTableViewCell
        let cellModel = baseCategory?.categories?[indexPath.row] as? NSDictionary
        let categoryModel = CategoryModel(dictionary: cellModel)
        cell.cellHeaderView.text = categoryModel?.categoryHeader
        cell.categoryList = categoryModel?.list
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if textView.isFirstResponder {
            self.textView.resignFirstResponder()
        }
    }
}

extension CategoryViewController: CategoryTableViewCellDelegate {
    
    func setAlarmLabel(alarmLabel: String) {
        self.alarmLabel = alarmLabel
        self.performSegue(withIdentifier: "toAlarmVC", sender: nil)
    }
}

extension CategoryViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        guard let text = textView.text else { return }
        placeholderLabel?.isHidden = !text.isEmpty
        self.alarmLabel = text
    }
}
