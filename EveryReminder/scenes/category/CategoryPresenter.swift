//
//  CategoryPresenter.swift
//  EveryReminder
//
//  Created by deneme on 14.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation
import FirebaseFirestore

class CategoryPresenter {
    
    private let categoryService : CategoryService
    weak private var categoryViewDelegate : CategoryViewDelegate?
    var db: Firestore!
    
    init(categoryService : CategoryService) {
        self.categoryService = categoryService
    }
    
    func setViewDelegate(categoryViewDelegate : CategoryViewDelegate?) {
        self.categoryViewDelegate = categoryViewDelegate
    }
    
    func getCategories() {
        db = Firestore.firestore()
        categoryService.getCategories(ref: db) { baseCategory in
            self.categoryViewDelegate?.setUI(category: baseCategory)
        }
        
        
    }
}
