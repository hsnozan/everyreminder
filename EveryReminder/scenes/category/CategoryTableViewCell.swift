//
//  CategoryTableViewCell.swift
//  EveryReminder
//
//  Created by deneme on 17.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var cellHeaderView: UILabel!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.dataSource = self
            self.collectionView.delegate = self
        }
    }
    
    var categoryList: NSMutableArray? {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    var delegate: CategoryTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension CategoryTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCollectionViewCell", for: indexPath as IndexPath) as! CategoryCollectionViewCell
        let cellModel = categoryList?[indexPath.row] as? NSDictionary
        let categoryCollectionModel = CategoryCollectionModel(dictionary: cellModel)
        cell.updateView()
        cell.setUI(model: categoryCollectionModel)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellModel = categoryList?[indexPath.row] as? NSDictionary
        let categoryCollectionModel = CategoryCollectionModel(dictionary: cellModel)
        delegate?.setAlarmLabel(alarmLabel: categoryCollectionModel.name ?? "")
    }
}
