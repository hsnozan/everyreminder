//
//  CategoryService.swift
//  EveryReminder
//
//  Created by deneme on 14.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation
import FirebaseFirestore

class CategoryService {
    
    func getCategories(ref: Firestore, callback: @escaping (BaseCategory) -> Void) {
        let document = ref.collection("categories").document("categoryList")
        document.getDocument { (document, error) in
            if let categoryModel = document.flatMap({
                $0.data().flatMap({ (data) in
                    return BaseCategory(dictionary: data)
                })
            }) {
                callback(categoryModel)
            } else {
                print("Document does not exist")
            }
        }
    }
    
}
