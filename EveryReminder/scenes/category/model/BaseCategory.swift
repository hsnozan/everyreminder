//
//  BaseCategory.swift
//  EveryReminder
//
//  Created by deneme on 18.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct BaseCategory {
    
    let categories: NSMutableArray?
    
    init(dictionary: [String: Any]) {
        self.categories = dictionary["categories"] as? NSMutableArray
    }
}
