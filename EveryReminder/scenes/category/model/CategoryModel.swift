//
//  CategoryModel.swift
//  EveryReminder
//
//  Created by deneme on 18.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct CategoryModel {
    
    let categoryHeader: String?
    let list: NSMutableArray?
    
    init?(dictionary: NSDictionary?) {
        self.categoryHeader = dictionary?["header"] as? String
        self.list = dictionary?["list"] as? NSMutableArray
    }
}
