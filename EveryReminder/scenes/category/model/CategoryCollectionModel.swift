//
//  CategoryCollectionModel.swift
//  EveryReminder
//
//  Created by deneme on 19.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct CategoryCollectionModel {
    
    let imageUrl: String?
    let name: String?
    
    init(dictionary: NSDictionary?) {
        self.imageUrl = dictionary?["imageUrl"] as? String
        self.name = dictionary?["name"] as? String
    }
}
