//
//  CategoryCollectionViewCell.swift
//  EveryReminder
//
//  Created by deneme on 17.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit
import Kingfisher

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    
    func updateView() {
        imageContainer.layer.cornerRadius = 10
        imageContainer.layer.masksToBounds = true
        imageContainer.layer.borderColor = UIColor.gray.cgColor
        imageContainer.layer.borderWidth = 0.5
        imageContainer.layer.masksToBounds = false
        imageContainer.clipsToBounds = false
    }
    
    func setUI(model: CategoryCollectionModel) {
        categoryName.text = model.name
        let imageUrl = URL(string: model.imageUrl ?? "")
        categoryImage.kf.setImage(with: imageUrl)
    }
}
