//
//  SplashPageViewController.swift
//  EveryReminder
//
//  Created by hsnozan on 3.09.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import UIKit

protocol SplashDelegate {
    func setReminderNowBtnClicked()
}

class SplashPageViewController: UIPageViewController, SplashDelegate {

    fileprivate var items: [UIViewController] = []
    private let SPLASH_PAGE_COUNT = 5
    
    private var splashDelegate: SplashDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        splashDelegate = self
        
        decoratePageControl()
        populateItems()
        if let firstViewController = items.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews {
            if view is UIScrollView {
                view.frame = UIScreen.main.bounds
            }
        }
    }
    
    fileprivate func populateItems() {
        let text = ["Organize your life with simple \n recurring reminders", "Water the flowers \n Every 10 days, 1 time", "Go to hairdresser \n Every 20 days, 1 time", "Make a cheat day \n Every week, 1 time", "Every, will remind all of your \n recurring tasks."]
        let images:[UIImage?] = [UIImage(named: "messyDoodle"), UIImage(named: "plantDoodle"), UIImage(named: "bikiniDoodle"), UIImage(named: "iceCreamDoodle"), UIImage(named: "logo")]
        
        for (index, t) in text.enumerated() {
            let c = createCarouselItemControler(with: t, with: images[index], isLastIndex: index == SPLASH_PAGE_COUNT - 1)
            items.append(c)
        }
    }
    
    fileprivate func decoratePageControl() {
        let pc = UIPageControl.appearance(whenContainedInInstancesOf: [SplashPageViewController.self])
        pc.currentPageIndicatorTintColor = .white
        pc.pageIndicatorTintColor = .lightGray
    }
    
    fileprivate func createCarouselItemControler(with titleText: String?, with image: UIImage?, isLastIndex: Bool?) -> UIViewController {
        let c = UIViewController()
        c.view = SplashItem(splashLabel: titleText, splashImage: image, isLastIndex: isLastIndex, delegate: splashDelegate)

        return c
    }
    
    
    func setReminderNowBtnClicked() {
        self.dismiss(animated: true) {
            self.performSegue(withIdentifier: "toHome", sender: nil)
        }
    }

}

extension SplashPageViewController: UIPageViewControllerDataSource {
    
    func presentationCount(for _: UIPageViewController) -> Int {
        return items.count
    }
    
    func presentationIndex(for _: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = items.firstIndex(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
    
    func pageViewController(_: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = items.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard items.count > previousIndex else {
            return nil
        }
        
        return items[previousIndex]
    }
    
    func pageViewController(_: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = items.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        guard items.count != nextIndex else {
            return nil
        }
        
        guard items.count > nextIndex else {
            return nil
        }
        
        return items[nextIndex]
    }
}
