//
//  SplashItem.swift
//  EveryReminder
//
//  Created by hsnozan on 3.09.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import UIKit

@IBDesignable
class SplashItem: UIView {
    
    @IBOutlet weak var splashItemImage: UIImageView!
    @IBOutlet weak var splashItemLabel: UILabel!
    @IBOutlet var splashContainer: UIView!
    @IBOutlet weak var setReminderNowBtn: UIButton!
    
    private var splashDelegate: SplashDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initWithNib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initWithNib()
    }
    
    convenience init(splashLabel: String? = "", splashImage: UIImage? = nil, isLastIndex: Bool? = false, delegate: SplashDelegate?) {
        self.init()
        splashItemLabel.text = splashLabel
        splashItemImage.image = splashImage
        updateUI(isLastIndex: isLastIndex)
        splashDelegate = delegate
    }
    
    private func initWithNib() {
        Bundle.main.loadNibNamed("SplashItem", owner: self, options: nil)
        splashContainer.frame = bounds
        splashContainer.autoresizingMask = [.flexibleHeight, .flexibleWidth
        ]
        addSubview(splashContainer)
    }
    
    private func updateUI(isLastIndex: Bool? = false) {
        setReminderNowBtn.layer.cornerRadius = 10
        setReminderNowBtn.isHidden = !(isLastIndex ?? false)
    }
    
    @IBAction func setReminderBtnTapped(_ sender: Any) {
        splashDelegate?.setReminderNowBtnClicked()
    }
}
