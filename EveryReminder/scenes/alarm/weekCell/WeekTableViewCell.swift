//
//  WeekTableViewCell.swift
//  EveryReminder
//
//  Created by deneme on 30.11.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

class WeekTableViewCell: UITableViewCell {
    
    @IBOutlet weak var weekDayLabel: UILabel!
    @IBOutlet weak var weekTimeLabel: UILabel!
    @IBOutlet weak var weekOrderLabel: UILabel!
    
    var weekDaysAction: (() -> Void)?
    var weekTimeAction: (() -> Void)?
    var alarmListModel: AlarmWeekModel? {
        didSet {
            setUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setUI() {
        if let weekDay = alarmListModel?.selectedDay {
            weekDayLabel.text = weekDay
            weekDayLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            weekDayLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
        
        if let timeAndHour = alarmListModel?.selectedHour {
            weekTimeLabel.text = timeAndHour
            weekTimeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            weekTimeLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
    }
    
    @IBAction func weekDayButtonTapped(_ sender: Any) {
        weekDaysAction?()
    }
    @IBAction func timeButtonTapped(_ sender: Any) {
        weekTimeAction?()
    }
}
