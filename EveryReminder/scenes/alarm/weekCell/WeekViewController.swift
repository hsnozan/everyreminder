//
//  WeekViewController.swift
//  EveryReminder
//
//  Created by hsnozan on 11.01.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import UIKit

class WeekViewController: UIViewController {

    @IBOutlet weak var hourMinutePicker: UIDatePicker!
    @IBOutlet weak var weekdaysPicker: UIPickerView!
    var weekdaysPickerData: [[String]] = [[String]]()
    var weekDays = [String]()
    var isFromWeekDaysBtn: Bool?
    var selectedDay: String?
    weak var alarmDayHoursDelegate: DayTimeDelegate?
    var indexPath: Int?
    var selectedDate: String?
    var selectedHour: String?
    var pickerDate: Date?
    var selectedFirstDayDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weekdaysPicker.dataSource = self
        weekdaysPicker.delegate = self
        weekDays = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday", "Sunday"]
        weekdaysPickerData = [weekDays]
        hideOrVisiblePicker(isFromWeekDaysBtn: isFromWeekDaysBtn ?? true)
    }

    func hideOrVisiblePicker(isFromWeekDaysBtn: Bool) {
        hourMinutePicker.isHidden = isFromWeekDaysBtn
        weekdaysPicker.isHidden = !isFromWeekDaysBtn
    }
    
    @IBAction func chooseBtnTapped(_ sender: Any) {
        if isFromWeekDaysBtn ?? true {
            alarmDayHoursDelegate?.choosenWeekDay(day: selectedDay ?? "Monday", notificationId: indexPath ?? 0)
        } else {
            if pickerDate == nil {
                formatDates(by: Date())
            }
            
            alarmDayHoursDelegate?.choosenWeekDayTime(selectedHour: selectedHour ?? "",pickerDate: pickerDate ?? Date(), notificationId: indexPath ?? 0)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    private func formatDates(by date: Date) {
        let dateFormatterHour = DateFormatter()
        dateFormatterHour.dateFormat = "HH:mm"
        
        self.pickerDate = hourMinutePicker.date
        selectedHour = dateFormatterHour.string(from: hourMinutePicker.date)
    }
}

extension WeekViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return weekDays.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as? UILabel;
        if (pickerLabel == nil) {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Montserrat", size: 12)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        pickerLabel?.text = weekdaysPickerData[component][row]
        return pickerLabel!;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedDay = weekdaysPickerData[component][row]
    }
    
}
