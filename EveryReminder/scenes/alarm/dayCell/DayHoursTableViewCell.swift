//
//  DayHoursTableViewCell.swift
//  EveryReminder
//
//  Created by deneme on 28.11.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

class DayHoursTableViewCell: UITableViewCell {

    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setChosenDateLabel(text: String?) -> Bool {
        if let selectedDate = text, selectedDate != "" {
            self.timeLabel.text = selectedDate
            self.timeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            return true
            
        } else {
            self.timeLabel.text = "Choose Time"
            self.timeLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            return false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func chooseTimeBtnTapped(_ sender: Any) {
    }
}
