//
//  DayHoursViewController.swift
//  EveryReminder
//
//  Created by deneme on 28.11.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

class DayHoursViewController: UIViewController {

    @IBOutlet weak var timePickerView: UIDatePicker!
    @IBOutlet weak var pickerContainer: UIView!
    
    weak var alarmDayHoursDelegate: DayTimeDelegate?
    var indexPath: IndexPath?
    var selectedDate: String?
    var selectedHour: String?
    var pickerDate: Date?
    var selectedFirstDayDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerContainer.layer.cornerRadius = 10
        pickerContainer.layer.masksToBounds = true
        timePickerView.minimumDate = Date()
        checkCellDates()
    }
    
    func checkCellDates() {
        if let cellDate = selectedFirstDayDate, indexPath?.row != 0 {
            timePickerView.minimumDate = Calendar.current.date(byAdding: .minute, value: 1, to: cellDate)
            timePickerView.maximumDate = Calendar.current.date(byAdding: .day, value: 1, to: cellDate)
        }
    }

    @IBAction func chooseBtnTapped(_ sender: Any) {
        if pickerDate == nil {
            formatDates(by: Date())
        }
        
        alarmDayHoursDelegate?.choosenTime(selectedHour: selectedHour ?? "",pickerDate: pickerDate ?? Date(), notificationId: indexPath?.row ?? 0)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dismissBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func formatDates(by date: Date) {
        let dateFormatterHour = DateFormatter()
        dateFormatterHour.dateFormat = "HH:mm"
        
        self.pickerDate = timePickerView.date
        selectedHour = dateFormatterHour.string(from: timePickerView.date)
    }
}
