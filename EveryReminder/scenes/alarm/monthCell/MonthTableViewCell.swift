//
//  MonthTableViewCell.swift
//  EveryReminder
//
//  Created by deneme on 30.11.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

class MonthTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var monthDayBtn: UIButton!
    @IBOutlet weak var monthTimeBtn: UIButton!
    
    var monthDaysAction: (() -> Void)?
    var monthTimeAction: (() -> Void)?
    var alarmListModel: AlarmMonthModel? {
        didSet {
            setUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUI() {
        if let monthDay = alarmListModel?.selectedDay {
            dayLabel.text = monthDay
            dayLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            dayLabel.text = "Day of the Month"
            dayLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
        
        if let timeAndHour = alarmListModel?.selectedHour {
            timeLabel.text = timeAndHour
            timeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            timeLabel.text = "Time of the Day"
            timeLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
    }
    
    @IBAction func monthDayBtn(_ sender: Any) {
        monthDaysAction?()
    }
    
    @IBAction func monthTimeBtnTapped(_ sender: Any) {
        monthTimeAction?()
    }
}
