//
//  MonthViewController.swift
//  EveryReminder
//
//  Created by hsnozan on 5.02.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import UIKit

class MonthViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var daysOfMonthPicker: UIPickerView!
    
    var monthDaysPickerData: [[String]] = [[String]]()
    var monthDays = [String]()
    var isFromMonthDayHoursBtn: Bool?
    var selectedMonthDay: String?
    var selectedMonthDayRow: Int? = 0
    weak var alarmMonthDayDelegate: MonthDayTimeDelegate?
    var indexPath: Int?
    var selectedDate: String?
    var selectedHour: String?
    var pickerDate: Date?
    var selectedFirstDayDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        daysOfMonthPicker.delegate = self
        daysOfMonthPicker.dataSource = self
        hideOrVisiblePicker(isFromMonthDayHoursBtn: isFromMonthDayHoursBtn ?? true)
        
        monthDays = ["1st day of the month","2nd day of the month","3rd day of the month","4th day of the month","5th day of the month","6th day of the month", "7th day of the month", "8th day of the month", "9th day of the month", "10th day of the month", "11th day of the month", "12th day of the month", "13th day of the month", "14th day of the month", "15th day of the month", "16th day of the month", "17th day of the month", "18th day of the month", "19th day of the month", "20th day of the month", "21st day of the mont", "22nd day of the month", "23rd day of the month", "24th day of the month", "25th day of the month", "26th day of the month", "27th day of the month", "28th day of the month", "29th day of the month", "30th day of the month", "31st day of the month"]
        monthDaysPickerData = [monthDays]
    }
    
    @IBAction func chooseBtnTapped(_ sender: Any) {
        if isFromMonthDayHoursBtn ?? true {
            alarmMonthDayDelegate?.choosenMonthDay(day: selectedMonthDay ?? "First day of the Month", notificationId: indexPath ?? 0, selectedMonthDayRow: (selectedMonthDayRow ?? 0) + 1)
        } else {
            if pickerDate == nil {
                formatDates(by: Date())
            }
            
            alarmMonthDayDelegate?.choosenMonthDayTime(selectedHour: selectedHour ?? "",pickerDate: pickerDate ?? Date(), notificationId: indexPath ?? 0)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    private func formatDates(by date: Date) {
        let dateFormatterHour = DateFormatter()
        dateFormatterHour.dateFormat = "HH:mm"
        
        self.pickerDate = datePicker.date
        selectedHour = dateFormatterHour.string(from: datePicker.date)
    }
    
    func hideOrVisiblePicker(isFromMonthDayHoursBtn: Bool) {
        datePicker.isHidden = isFromMonthDayHoursBtn
        daysOfMonthPicker.isHidden = !isFromMonthDayHoursBtn
    }
}

extension MonthViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return monthDays.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as? UILabel;
        if (pickerLabel == nil) {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Montserrat", size: 12)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        pickerLabel?.text = monthDaysPickerData[component][row]
        return pickerLabel!;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedMonthDay = monthDaysPickerData[component][row]
        selectedMonthDayRow = row
    }
    
}
