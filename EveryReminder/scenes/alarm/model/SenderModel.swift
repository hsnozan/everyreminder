//
//  SenderModel.swift
//  EveryReminder
//
//  Created by hsnozan on 24.05.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct SenderModel {
    var isDayButton: Bool?
    var indexpathRow: Int?
    
    init(isDayButton: Bool? = nil, indexpathRow: Int? = nil) {
        self.isDayButton = isDayButton
        self.indexpathRow = indexpathRow
    }
}
