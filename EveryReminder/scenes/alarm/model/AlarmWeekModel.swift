//
//  AlarmWeekModel.swift
//  EveryReminder
//
//  Created by hsnozan on 13.01.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct AlarmWeekModel {
    var selectedHour: String?
    var selectedDay: String?
    var id: Int?
    var pickerDate: Date?
    
    init(selectedDay: String? = nil, selectedHour: String? = nil, id: Int? = nil, pickerDate: Date? = nil) {
        self.id = id
        self.selectedHour = selectedHour
        self.selectedDay = selectedDay
        self.pickerDate = pickerDate
    }
}
