//
//  AlarmModel.swift
//  EveryReminder
//
//  Created by deneme on 4.11.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct AlarmListModel {
    var selectedHour: String?
    var selectedDate: String?
    var fullDateLabel: String?
    var id: Int?
    
    init(selectedHour: String? = nil, selectedDate: String? = nil, id: Int? = nil) {
        self.id = id
        self.selectedHour = selectedHour
        self.selectedDate = selectedDate
        
        if let selectedDate = self.selectedDate, let selectedHour = self.selectedHour {
            self.fullDateLabel = selectedDate + " - " + selectedHour
        }
        
    }
}
