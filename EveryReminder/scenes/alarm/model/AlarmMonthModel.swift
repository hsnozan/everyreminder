//
//  AlarmMonthModel.swift
//  EveryReminder
//
//  Created by hsnozan on 24.05.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct AlarmMonthModel {
    var selectedHour: String?
    var selectedDay: String?
    var id: Int?
    var pickerDate: Date?
    var selectedMonthDayRow: Int?
    
    init(selectedDay: String? = nil, selectedHour: String? = nil, id: Int? = nil, pickerDate: Date? = nil, selectedMonthDayRow: Int? = nil) {
        self.id = id
        self.selectedHour = selectedHour
        self.selectedDay = selectedDay
        self.pickerDate = pickerDate
        self.selectedMonthDayRow = selectedMonthDayRow
    }
}
