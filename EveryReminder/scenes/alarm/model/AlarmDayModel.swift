//
//  AlarmDayModel.swift
//  EveryReminder
//
//  Created by hsnozan on 11.01.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct AlarmDayModel {
    var selectedHour: String?
    var id: Int?
    
    init(selectedHour: String? = nil, id: Int? = nil) {
        self.id = id
        self.selectedHour = selectedHour
    }
}
