//
//  NotificationModel.swift
//  EveryReminder
//
//  Created by deneme on 29.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct NotificationModel {
    
    var id: String?
    var title: String?
    var notificationDate: Date?
    var notificationDay: Int?
    var notificationMonth: Int?
    var notificationTime: String?
    var alarmSubtitle: String?
    
    init(dictionary: NSDictionary?) {
        self.id = dictionary?["id"] as? String
        self.title = dictionary?["title"] as? String
        self.notificationDate = dictionary?["notificationDate"] as? Date
        self.notificationDay = dictionary?["notificationDay"] as? Int
        self.notificationMonth = dictionary?["notificationMonth"] as? Int
        self.notificationTime = dictionary?["notificationTime"] as? String
        self.alarmSubtitle = dictionary?["alarmSubtitle"] as? String
    }
}
