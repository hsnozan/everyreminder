//
//  SenderYearModel.swift
//  EveryReminder
//
//  Created by hsnozan on 9.07.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct SenderYearModel {
    var isFromMonthBtn: Bool?
    var isFromYearTimeBtn : Bool?
    var indexpathRow: Int?
    
    init(isFromMonthBtn: Bool? = nil, indexpathRow: Int? = nil, isFromYearTimeBtn : Bool? = nil) {
        self.isFromMonthBtn = isFromMonthBtn
        self.isFromYearTimeBtn = isFromYearTimeBtn
        self.indexpathRow = indexpathRow
    }
}
