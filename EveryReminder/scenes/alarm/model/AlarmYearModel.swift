//
//  AlarmYearModel.swift
//  EveryReminder
//
//  Created by hsnozan on 8.07.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct AlarmYearModel {
    
    var selectedDay: Int?
    var selectedMonth: Int?
    var selectedTime: String?
    var id: Int?
    var pickerDate: Date?
    
    init(selectedDay: Int? = nil, selectedMonth: Int? = nil, id: Int? = nil, pickerDate: Date? = nil) {
        self.id = id
        self.selectedMonth = selectedMonth
        self.selectedDay = selectedDay
        self.pickerDate = pickerDate
    }
}
