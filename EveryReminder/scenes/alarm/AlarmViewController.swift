//
//  AlarmViewController.swift
//  EveryReminder
//
//  Created by deneme on 19.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit
import UserNotifications
import AVFoundation

protocol AlarmViewDelagate: AnyObject {
    func reloadTableView()
    func untilThisDate(pickerDate: Date)
    func chooseDate(pickerDate: Date)
    func notificationsAdded()
    func showAlarm()
    func showShouldChooseDayFirstError()
}

class AlarmViewController: UIViewController, AlarmChooseDelagate {
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var timesLabel: UILabel!
    @IBOutlet weak var alarmTableView: ContentSizedTableView!
    @IBOutlet weak var alarmTitle: UILabel!
    @IBOutlet weak var customPickerContainerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContent: UIView!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var doesntEndRadioBtn: RadioButton!
    @IBOutlet weak var untilThisDateRadioBtn: RadioButton!
    @IBOutlet weak var repeatCountRadioButton: RadioButton!
    @IBOutlet weak var chooseDateBtn: RadioButton!
    @IBOutlet weak var fromNowOnBtn: RadioButton!
    @IBOutlet weak var repeatCountContainer: UIView!
    @IBOutlet weak var repeatCountTextField: UITextField!
    @IBOutlet weak var chooseDateContainerView: UIView!
    @IBOutlet weak var untilDateContainerView: UIView!
    @IBOutlet weak var repeatContainerView: UIView!
    @IBOutlet weak var soundLabel: UILabel!
    @IBOutlet weak var pickerTimesLabel: UILabel!
    
    private let alarmPresenter = AlarmPresenter(alarmService: AlarmService())
    weak var alarmViewDelegate: AlarmViewDelagate?
    var selectedDayTime: String? = ""
    var selectedAlarmLabel: String? = ""
    var selectedFirstCellDate: Date?
    var userId: String? = ""
    var oneTimeSelected: Bool? = false
    var oneDateSelected: Bool? = false
    var alarms: NSMutableArray?
    var cellType: CellType? = .minuteType
    weak var alarmChooseDelegate: AlarmChooseDelagate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        registerCells()
        registerParameters()
    }
    
    func registerCells() {
        let dayHoursNib = UINib(nibName: "DayHoursTableViewCell", bundle: nil)
        alarmTableView.register(dayHoursNib, forCellReuseIdentifier: "dayHoursTableViewCell")
        
        let weekNib = UINib(nibName: "WeekTableViewCell", bundle: nil)
        alarmTableView.register(weekNib, forCellReuseIdentifier: "weekTableViewCell")
        
        let monthNib = UINib(nibName: "MonthTableViewCell", bundle: nil)
        alarmTableView.register(monthNib, forCellReuseIdentifier: "monthTableViewCell")
        
        let yearNib = UINib(nibName: "YearTableViewCell", bundle: nil)
        alarmTableView.register(yearNib, forCellReuseIdentifier: "yearTableViewCell")
    }
    
    func registerParameters() {
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.alarmViewDelegate = self
        self.alarmChooseDelegate = self
        self.alarmTableView.rowHeight = 75
        alarmTitle.text = selectedAlarmLabel
        alarmPresenter.setArrays()
        alarmPresenter.setViewDelegate(alarmViewDelegate: alarmViewDelegate)
        alarmPresenter.requestAuth()
        alarmPresenter.registerCategories(vc: self)
        alarmPresenter.appendFirstItemToModels()
        alarmPresenter.setUserID(userId: userId, alarms: alarms ?? [], alarmLabel: selectedAlarmLabel ?? "")
        setPickerContainerView()
        setRadioButtons()
        doesntEndRadioBtn.isSelected = true
        fromNowOnBtn.isSelected = true
        repeatCountTextField.delegate = self
        repeatCountTextField.addTarget(self, action: #selector(AlarmViewController.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
    }
    
    @IBAction func setReminderButtonTapped(_ sender: Any) {
        alarmPresenter.scheduleNotification()
    }
    
    override func viewDidLayoutSubviews(){
        alarmTableView.frame = CGRect(x: alarmTableView.frame.origin.x, y: alarmTableView.frame.origin.y, width: alarmTableView.frame.size.width, height: alarmTableView.contentSize.height)
        alarmTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        alarmTableView.frame = CGRect(x: alarmTableView.frame.origin.x, y: alarmTableView.frame.origin.y, width: alarmTableView.frame.size.width, height: alarmTableView.contentSize.height)
    }
    
    func setPickerContainerView() {
        customPickerContainerView.layer.cornerRadius = 10
        customPickerContainerView.layer.masksToBounds = true
        customPickerContainerView.layer.borderColor = UIColor.gray.cgColor
        customPickerContainerView.layer.borderWidth = 0.5
        customPickerContainerView.layer.masksToBounds = false
        customPickerContainerView.clipsToBounds = false
        
        buttonContainerView.layer.cornerRadius = 25
        buttonContainerView.layer.masksToBounds = true
        buttonContainerView.clipsToBounds = false
    }
    
    func setRadioButtons() {
        doesntEndRadioBtn.alternateButton = [untilThisDateRadioBtn, repeatCountRadioButton]
        untilThisDateRadioBtn.alternateButton = [doesntEndRadioBtn, repeatCountRadioButton]
        repeatCountRadioButton.alternateButton = [untilThisDateRadioBtn, doesntEndRadioBtn]
    }
    
    func setNavigationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(CategoryViewController.backButtonTapped))
        navigationItem.leftBarButtonItem?.tintColor = .white
        navigationItem.leftBarButtonItem?.isEnabled = true
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let popupVC = segue.destination as? AlarmPopupViewController,segue.identifier == "toPopupVc" {
            popupVC.alarmViewDelegate = alarmViewDelegate
            popupVC.isFromRadioBtn = false
            popupVC.selectedType = alarmPresenter.selectedType
            popupVC.selectedFirstCellDate = self.selectedFirstCellDate
            popupVC.indexPath = self.alarmTableView.indexPathForSelectedRow
        } else  if let popupVC = segue.destination as? AlarmPopupViewController,segue.identifier == "toPopupVCFromRadioBtn" {
            popupVC.alarmViewDelegate = alarmViewDelegate
            popupVC.isFromRadioBtn = true
            popupVC.isSelectedTypeYearAndMonth = (alarmPresenter.selectedType == "Year" || alarmPresenter.selectedType == "Month")
        } else if let alarmSoundsVC = segue.destination as? AlarmSoundsViewController,segue.identifier == "toAlarmSounds" {
            alarmSoundsVC.alarmChooseDelegate = alarmChooseDelegate
        } else  if let popupVC = segue.destination as? AlarmPopupViewController,segue.identifier == "toPopupVCFromChooseRadioBtn" {
            popupVC.alarmViewDelegate = alarmViewDelegate
            popupVC.isFromChooseDateRadioBtn = true
        } else  if let dayHoursVC = segue.destination as? DayHoursViewController,segue.identifier == "toDayHoursVc" {
            dayHoursVC.alarmDayHoursDelegate = alarmPresenter
            dayHoursVC.selectedFirstDayDate = alarmPresenter.selectedFirstDayDate
            dayHoursVC.indexPath = self.alarmTableView.indexPathForSelectedRow
        } else if let weekVC = segue.destination as? WeekViewController, (segue.identifier == "toWeekVCFromWeekDays" || segue.identifier == "toWeekVCFromWeekTime"), let sender = sender as? SenderModel {
            let isFromWeekDaysBtn = sender.isDayButton
            let indexPath = sender.indexpathRow
            weekVC.isFromWeekDaysBtn = isFromWeekDaysBtn
            weekVC.alarmDayHoursDelegate = alarmPresenter
            weekVC.indexPath = indexPath
        } else if let monthVC = segue.destination as? MonthViewController, let sender = sender as? SenderModel, segue.identifier == "toMonthVC"{
            let isFromMonthDayHoursBtn = sender.isDayButton
            let indexPath = sender.indexpathRow
            monthVC.isFromMonthDayHoursBtn = isFromMonthDayHoursBtn
            monthVC.alarmMonthDayDelegate = alarmPresenter
            monthVC.indexPath = indexPath
        } else if let yearVC = segue.destination as? YearViewController, let sender = sender as? SenderYearModel {
            if segue.identifier == "showYearVCFromTime" {
                yearVC.isFromYearTimeBtn = sender.isFromYearTimeBtn
            } else if segue.identifier == "showYearVC" {
                yearVC.isFromMonthBtn = sender.isFromMonthBtn
            }
            yearVC.indexPath = sender.indexpathRow
            yearVC.alarmYearDelegate = alarmPresenter
        }
    }
    
    @IBAction func doesntEndRadioBtnTapped(_ sender: Any) {
        selectFromNowOnBtn()
        selectFirstRow()
        selectDoesntEndBtn()
    }
    @IBAction func untilThisDateBtnTapped(_ sender: Any) {
        if isSelectedTypeConfirmed() {
            doesntEndRadioBtn.isSelected = false
            untilThisDateRadioBtn.isSelected = true
            repeatCountRadioButton.isSelected = false
            repeatCountContainer.isHidden = true
            alarmPresenter.radioButtonType = RadioButtonType.untilThisDateRadioBtn
            performSegue(withIdentifier: "toPopupVCFromRadioBtn", sender: nil)
        }
    }
    @IBAction func repeatCountBtnTapped(_ sender: Any) {
        if isSelectedTypeConfirmed() {
            doesntEndRadioBtn.isSelected = false
            untilThisDateRadioBtn.isSelected = false
            repeatCountRadioButton.isSelected = true
            repeatCountContainer.isHidden = false
            alarmPresenter.radioButtonType = RadioButtonType.repeatCountRadioButton
        }
    }
    
    @IBAction func chooseDateBtnTapped(_ sender: Any) {
        if !doesntEndRadioBtn.isSelected {
            fromNowOnBtn.isSelected = false
            chooseDateBtn.isSelected = true
            alarmPresenter.dateRadioButtonType = DateRadioButtonType.chooseDateRadioBtn
            performSegue(withIdentifier: "toPopupVCFromChooseRadioBtn", sender: nil)
        } else {
            let alert = UIAlertController(title: "Alert", message: "Repeat Type shouldn't be doesn't end", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func fromNowOnBtnTapped(_ sender: Any) {
        selectFromNowOnBtn()
    }
    
    private func selectDoesntEndBtn() {
        doesntEndRadioBtn.isSelected = true
        untilThisDateRadioBtn.isSelected = false
        repeatCountRadioButton.isSelected = false
        repeatCountContainer.isHidden = true
        alarmPresenter.radioButtonType = RadioButtonType.doesntEndRadioBtn
    }
    
    func selectFirstRow() {
        pickerView.selectRow(0, inComponent: 2, animated: true)
        self.pickerView(self.pickerView, didSelectRow: 0, inComponent: 2)
    }
    
    func selectFromNowOnBtn() {
        fromNowOnBtn.isSelected = true
        chooseDateBtn.isSelected = false
        alarmPresenter.dateRadioButtonType = DateRadioButtonType.fromNowOnRadioBtn
    }
    
    func updateCellType(rowValue: String) {
        cellType = CellType(rawValue: rowValue)
        reloadTableView()
    }
    
    func choosenSound(soundLabel: String?, soundName: String?) {
        self.soundLabel.text = soundLabel
        self.soundLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        alarmPresenter.soundName = soundName
    }
    
    private func isSelectedTypeConfirmed() -> Bool {
        switch alarmPresenter.selectedType {
        case "Day","Week","Month","Year":
            alarmTableView.reloadData()
            return true
        default:
            showOnlyAvailableError()
            return false
        }
    }
    
    private func showOnlyAvailableError() {
        let alert = UIAlertController(title: "Alert", message: "Only available for Day, Week, Month and Year", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: EXTENSIONS

extension AlarmViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return alarmPresenter.pickerData[component].count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as? UILabel;
        if (pickerLabel == nil) {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Montserrat", size: 12)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        pickerLabel?.text = alarmPresenter.pickerData[component][row]
        return pickerLabel!;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return alarmPresenter.pickerData[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let componentValue = component
        oneTimeSelected = false
        oneDateSelected = false
        
        if componentValue == 0, alarmPresenter.pickerData[component][row] != "1" {
            pickerView.selectRow(0, inComponent: 2, animated: true)
            oneTimeSelected = true
        } else if componentValue == 1 {
            updateCellType(rowValue: alarmPresenter.pickerData[component][row])
        } else if componentValue == 2, alarmPresenter.pickerData[component][row] != "1" {
            pickerView.selectRow(0, inComponent: 0, animated: true)
            oneDateSelected = true
        }
        
        alarmPresenter.pickerRowSelected(component: component, row: row)
        let value = alarmPresenter.selectedType
        if  value == "Minute" || value == "Hour" || doesntEndRadioBtn.isSelected {
            pickerView.selectRow(0, inComponent: 2, animated: true)
            oneTimeSelected = true
        }
        
        if oneTimeSelected ?? false {
            alarmPresenter.pickerRowSelected(component: 2, row: 0)
            pickerTimesLabel.text = "Time"
        } else {
            pickerTimesLabel.text = "Times"
        }
        
        if oneDateSelected ?? false {
            alarmPresenter.pickerRowSelected(component: 0, row: 0)
        }
        
        if componentValue == 2, alarmPresenter.pickerData[component][row] == "1" {
            pickerTimesLabel.text = "Time"
        }
    }
}

extension AlarmViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch alarmPresenter.selectedType {
        case "Day","Week","Month","Year":
            return doesntEndRadioBtn.isSelected ? 0 : alarmPresenter.alarmCount ?? 0
        default:
            selectDoesntEndBtn()
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cellType {
        case .dayType:
            let cell = tableView.dequeueReusableCell(withIdentifier: "dayHoursTableViewCell", for: indexPath) as? DayHoursTableViewCell
            let alarmListModel = alarmPresenter.selectedDayTimes[indexPath.row]
            cell?.orderLabel.text = String(indexPath.row + 1) + "."
            if cell?.setChosenDateLabel(text: alarmListModel.selectedHour) ?? true {
                self.selectedDayTime = ""
            }
            return cell ?? UITableViewCell()
        case .weekType:
            let cell = tableView.dequeueReusableCell(withIdentifier: "weekTableViewCell", for: indexPath) as? WeekTableViewCell
            let alarmListModel = alarmPresenter.selectedWeekDays[indexPath.row]
            cell?.alarmListModel = alarmListModel
            cell?.weekOrderLabel.text = String(indexPath.row + 1) + "."
            cell?.weekDaysAction = {[unowned self] in
                let senderModel = SenderModel(isDayButton: true, indexpathRow: indexPath.row)
                self.performSegue(withIdentifier: "toWeekVCFromWeekDays", sender: senderModel)
            }
            cell?.weekTimeAction = {[unowned self] in
                let senderModel = SenderModel(isDayButton: false, indexpathRow: indexPath.row)
                if (cell?.alarmListModel?.selectedDay) != nil {
                    self.performSegue(withIdentifier: "toWeekVCFromWeekTime", sender: senderModel)
                }else {
                    self.showShouldChooseDayFirstError()
                }
            }
            return cell ?? UITableViewCell()
        case .monthType:
            let cell = tableView.dequeueReusableCell(withIdentifier: "monthTableViewCell", for: indexPath) as? MonthTableViewCell
            let alarmListModel = alarmPresenter.selectedMonthDays[indexPath.row]
            cell?.alarmListModel = alarmListModel
            cell?.orderLabel.text = String(indexPath.row + 1) + "."
            cell?.monthDaysAction = {[unowned self] in
                let senderModel = SenderModel(isDayButton: true, indexpathRow: indexPath.row)
                self.performSegue(withIdentifier: "toMonthVC", sender: senderModel)
            }
            cell?.monthTimeAction = {[unowned self] in
                let senderModel = SenderModel(isDayButton: false, indexpathRow: indexPath.row)
                if (cell?.alarmListModel?.selectedDay) != nil {
                    self.performSegue(withIdentifier: "toMonthVC", sender: senderModel)
                } else {
                    self.showShouldChooseDayFirstError()
                }
            }
            return cell ?? UITableViewCell()
        case .yearType:
            let cell = tableView.dequeueReusableCell(withIdentifier: "yearTableViewCell", for: indexPath) as? YearTableViewCell
            cell?.alarmYearModel = alarmPresenter.selectedYearDayAndTimes[indexPath.row]
            cell?.orderLabel.text = String(indexPath.row + 1) + "."
            cell?.yearDayAction = {[unowned self] in
                let senderModel = SenderYearModel(isFromMonthBtn: false, indexpathRow: indexPath.row)
                self.performSegue(withIdentifier: "showYearVC", sender: senderModel)
            }
            cell?.yearMonthAction = {[unowned self] in
                let senderModel = SenderYearModel(isFromMonthBtn: true, indexpathRow: indexPath.row)
                self.performSegue(withIdentifier: "showYearVC", sender: senderModel)
            }
            cell?.yearTimeAction = {[unowned self] in
                let senderModel = SenderYearModel(indexpathRow: indexPath.row, isFromYearTimeBtn: true)
                self.performSegue(withIdentifier: "showYearVCFromTime", sender: senderModel)
            }
            return cell ?? UITableViewCell()
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard tableView.cellForRow(at: indexPath) != nil else { return }
        
        switch cellType {
        case .dayType:
            performSegue(withIdentifier: "toDayHoursVc", sender: nil)
        default:
            break
        }
    }
}

extension AlarmViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else {return}
        alarmPresenter.repeatCount = Int(text)
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        guard let text = textField.text else {return}
        alarmPresenter.repeatCount = Int(text)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == repeatCountTextField {
            textField.resignFirstResponder()
            return false
        }
        return true
    }
}

extension AlarmViewController: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let customData = userInfo["customData"] as? String {
            print("Custom data received: \(customData)")
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                print("Default identifier")
            case "show":
                print("Show more information…")
                break
                
            default:
                break
            }
        }
        completionHandler()
    }
}

extension AlarmViewController: AlarmViewDelagate {
    
    func reloadTableView() {
        self.alarmTableView.reloadData()
    }
    
    func untilThisDate(pickerDate: Date) {
        alarmPresenter.setUntilThisDate(pickerDate: pickerDate)
    }
    
    func chooseDate(pickerDate: Date) {
        alarmPresenter.setChooseDate(pickerDate: pickerDate)
    }
    
    func notificationsAdded() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showAlarm() {
        let alert = UIAlertController(title: "Alert", message: "Check for your alarm", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showShouldChooseDayFirstError() {
        let alert = UIAlertController(title: "Alert", message: "You should choose the day first", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
