//
//  AlarmPresenter.swift
//  EveryReminder
//
//  Created by deneme on 22.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation
import EventKit
import UserNotifications

enum RadioButtonType: String {
    case doesntEndRadioBtn = "0"
    case untilThisDateRadioBtn = "1"
    case repeatCountRadioButton = "2"
}

enum DateRadioButtonType: String {
    case chooseDateRadioBtn = "0"
    case fromNowOnRadioBtn = "1"
}

enum CellType: String {
    case minuteType = "Minute"
    case hourType = "Hour"
    case dayType = "Day"
    case weekType = "Week"
    case monthType = "Month"
    case yearType = "Year"
}

protocol AlarmChooseDelagate: AnyObject {
    func choosenSound(soundLabel: String?, soundName: String?)
}

protocol DayTimeDelegate: AnyObject {
    func choosenTime(selectedHour: String, pickerDate: Date, notificationId: Int)
    func choosenWeekDayTime(selectedHour: String, pickerDate: Date, notificationId: Int)
    func choosenWeekDay(day: String, notificationId: Int)
}

protocol MonthDayTimeDelegate: AnyObject {
    func choosenMonthDayTime(selectedHour: String, pickerDate: Date, notificationId: Int)
    func choosenMonthDay(day: String, notificationId: Int, selectedMonthDayRow: Int)
}

protocol YearDelegate: AnyObject {
    func choosenYearDay(selectedDay: Int, notificationId: Int)
    func choosenYearMonth(selectedMonth: Int, pickerDate: Date, notificationId: Int)
    func choosenYearTime(selectedTime: String, pickerDate: Date, notificationId: Int)
}

class AlarmPresenter {
    
    private let alarmService : AlarmService
    weak private var alarmViewDelegate : AlarmViewDelagate?
    
    var pickerData: [[String]] = [[String]]()
    var daysAndTimes = [String]()
    var optionTypes = [String]()
    var times = [String]()
    let center = UNUserNotificationCenter.current()
    var selectedDate: String? = "1"
    var selectedType: String? = "Minute"
    var selectedTime: String? = "1"
    var notificationModels = NSMutableArray()
    var notificationWeekModels = NSMutableArray()
    var notificationMonthModels = NSMutableArray()
    var notificationYearModels = NSMutableArray()
    var radioButtonType: RadioButtonType? = .doesntEndRadioBtn
    var dateRadioButtonType: DateRadioButtonType? = .fromNowOnRadioBtn
    var repeatCount: Int?
    var userId: String? = ""
    var alarms: NSMutableArray?
    var notificationIds = [Int]()
    var untilDate: Date?
    var chooseDate: Date?
    var selectedAlarmLabel: String? = ""
    var soundName: String? = "iphone_notification"
    var selectedDayTimes = [AlarmDayModel]()
    var selectedWeekDays = [AlarmWeekModel]()
    var selectedMonthDays = [AlarmMonthModel]()
    var selectedYearDayAndTimes = [AlarmYearModel]()
    var selectedFirstDayDate: Date?
    
    var alarmCount: Int? {
        didSet {
            alarmViewDelegate?.reloadTableView()
        }
    }
    var beforeCount: Int? = 1
    
    init(alarmService : AlarmService) {
        self.alarmService = alarmService
    }
    
    func setViewDelegate(alarmViewDelegate : AlarmViewDelagate?) {
        self.alarmViewDelegate = alarmViewDelegate
    }
    
    func requestAuth() {
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                print("Yay!")
            } else {
                print("D'oh")
            }
        }
    }
    
    func setUserID(userId: String?, alarms: NSMutableArray, alarmLabel: String) {
        self.userId = userId
        self.alarms = alarms
        self.selectedAlarmLabel = alarmLabel
    }
    
    func setArrays() {
        daysAndTimes = ["1","2","3","4","5","6","7","8","9","10",
                        "11","12","13","14","15","16","17","18","19","20",
                        "21","22","23","24","25","26","27","28","29","30","31"]
        optionTypes = ["Minute","Hour","Day","Week","Month","Year"]
        pickerData = [daysAndTimes, optionTypes, daysAndTimes]
    }
    
    func pickerRowSelected(component: Int, row: Int) {
        let componentValue = component
        let chosenValue = pickerData[component][row]
        if componentValue == 0 {
            selectedDate = chosenValue
        } else if componentValue == 1 {
            selectedType = chosenValue
        } else {
            selectedTime = chosenValue
        }
        
        if let selectedTime = selectedTime {
            alarmCount = Int(selectedTime)
            if let beforeCount = beforeCount {
                if beforeCount > Int(selectedTime) ?? 1 {
                    removeAlarm()
                } else if beforeCount < Int(selectedTime) ?? 1 {
                    appendAlarm()
                }
            }
            beforeCount = alarmCount
        }
    }
    
    func appendFirstItemToModels() {
        selectedDayTimes.append(AlarmDayModel(id: 0))
        selectedWeekDays.append(AlarmWeekModel(id: 0))
        selectedMonthDays.append(AlarmMonthModel(id: 0))
        selectedYearDayAndTimes.append(AlarmYearModel(id: 0))
    }
    
    private func appendAlarm() {
        appendAlarmModel()
    }
    
    private func removeAlarm() {
        removeAlarmDayModel()
        removeWeekDayModel()
        removeMonthModel()
        removeYearModel()
    }
    
    private func appendAlarmModel() {
        for count in (beforeCount ?? 1) ..< (alarmCount ?? 1) {
            selectedDayTimes.append(AlarmDayModel(id: count))
            selectedWeekDays.append(AlarmWeekModel(id: count))
            selectedMonthDays.append(AlarmMonthModel(id: count))
            selectedYearDayAndTimes.append(AlarmYearModel(id: count))
        }
    }
    
    private func removeAlarmDayModel() {
        if notificationModels.count >= selectedDayTimes.count {
            for count in stride(from: notificationModels.count - 1, to: (alarmCount ?? 1) - 1, by: -1) {
                notificationModels.removeObject(at: count)
            }
        }
        for count in stride(from: selectedDayTimes.count - 1, to: (alarmCount ?? 1) - 1, by: -1) {
            selectedDayTimes.remove(at: count)
        }
    }
    
    private func removeWeekDayModel() {
        if notificationWeekModels.count >= selectedWeekDays.count {
            for count in stride(from: notificationWeekModels.count - 1, to: (alarmCount ?? 1) - 1, by: -1) {
                notificationWeekModels.removeObject(at: count)
            }
        }
        for count in stride(from: selectedWeekDays.count - 1, to: (alarmCount ?? 1) - 1, by: -1) {
            selectedWeekDays.remove(at: count)
        }
    }
    
    private func removeMonthModel() {
        if notificationMonthModels.count >= selectedMonthDays.count {
            for count in stride(from: notificationMonthModels.count - 1, to: (alarmCount ?? 1) - 1, by: -1) {
                notificationMonthModels.removeObject(at: count)
            }
        }
        for count in stride(from: selectedMonthDays.count - 1, to: (alarmCount ?? 1) - 1, by: -1) {
            selectedMonthDays.remove(at: count)
        }
    }
    
    private func removeYearModel() {
        if notificationYearModels.count >= selectedYearDayAndTimes.count {
            for count in stride(from: notificationYearModels.count - 1, to: (alarmCount ?? 1) - 1, by: -1) {
                notificationYearModels.removeObject(at: count)
            }
        }
        for count in stride(from: selectedYearDayAndTimes.count - 1, to: (alarmCount ?? 1) - 1, by: -1) {
            selectedYearDayAndTimes.remove(at: count)
        }
    }
    
    func setUntilThisDate(pickerDate: Date?) {
        untilDate = pickerDate
    }
    
    func setChooseDate(pickerDate: Date?) {
        chooseDate = pickerDate
    }
    
    func scheduleNotification() {
        if isFineAlarm() {
            guard let selectedType = selectedType else { return }
            guard let selectedDateTime = selectedDate else { return }
            if notificationModels.count != 0 {
                createAlarmFromNotificationModels(selectedType: selectedType, selectedDateTime: selectedDateTime, listOfModels: notificationModels)
            } else if notificationMonthModels.count != 0 {
                createAlarmFromNotificationModels(selectedType: selectedType, selectedDateTime: selectedDateTime, listOfModels: notificationMonthModels)
            } else if notificationWeekModels.count != 0 {
                createAlarmFromNotificationModels(selectedType: selectedType, selectedDateTime: selectedDateTime, listOfModels: notificationWeekModels)
            } else if notificationYearModels.count != 0 {
                createAlarmFromNotificationModels(selectedType: selectedType, selectedDateTime: selectedDateTime, listOfModels: notificationYearModels)
            } else {
                createAlarmFromDoesntEnd(selectedType: selectedType, selectedDateTime: selectedDateTime)
            }
        }
    }
    
    private func isFineAlarm() -> Bool {
        if radioButtonType == RadioButtonType.doesntEndRadioBtn {
            return true
        } else if notificationModels.count == 0 && (selectedType == "Day") {
            showAlert()
            return false
        } else if notificationWeekModels.count == 0 && (selectedType == "Week") {
            showAlert()
            return false
        } else if notificationMonthModels.count == 0 && (selectedType == "Month") {
            showAlert()
            return false
        } else if notificationYearModels.count == 0 && (selectedType == "Year") {
            showAlert()
            return false
        } else if radioButtonType == .untilThisDateRadioBtn  && untilDate == nil {
            showAlert()
            return false
        } else if radioButtonType == .repeatCountRadioButton && repeatCount == nil {
            showAlert()
            return false
        } else if selectedType == "Minute" || selectedType == "Hour" || dateRadioButtonType == DateRadioButtonType.fromNowOnRadioBtn {
            return true
        } else {
            return true
        }
    }
    
    private func showAlert() {
        alarmViewDelegate?.showAlarm()
    }
    
    func createAlarmFromNotificationModels(selectedType: String, selectedDateTime: String, listOfModels: NSMutableArray) {
        for notification in listOfModels {
            var notificationDictionary = notification as? NSDictionary
            let notificationModel = NotificationModel(dictionary: notificationDictionary)
            let content      = UNMutableNotificationContent()
            content.body = "It is time to: " + (notificationModel.title ?? "")
            content.categoryIdentifier = "alarm"
            content.userInfo = ["customData": "fizzbuzz"]
            content.sound    = UNNotificationSound(named: UNNotificationSoundName(rawValue: (soundName ?? "") + ".mp3"))
            
            let createAlarmModel = CreateAlarm(center: UNUserNotificationCenter.current())
            var unit : Calendar.Component
            unit = createAlarmModel.unitType(selectedType: selectedType)
            
            var triggerComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: notificationModel.notificationDate ?? Date())
            triggerComponents.calendar = Calendar.current
            let notificationId = notificationModel.id ?? UUID().uuidString
            switch radioButtonType ?? .doesntEndRadioBtn {
            case .doesntEndRadioBtn:
                createAlarmModel.scheduleDoesntEndReminders(content: content, fromDate: selectedDate ?? "", unit: unit, id: notificationId, selectedNotificationDay:  Date().get(.day), notificationDate: Date())
                notificationDictionary = createAlarmModel.setDoesntEndDictionary(content: content, fromDate: triggerComponents, unit: unit, notificationDate: Date(), selectedAlarmLabel: selectedAlarmLabel ?? "Alarm", soundName: soundName ?? "", selectedDate: selectedDate, id: notificationId, alarmSubtitle: notificationModel.alarmSubtitle ?? "", notificationDay:  Date().get(.day))
            case .untilThisDateRadioBtn:
                if let untilDate = untilDate {
                    if unit == .year && triggerComponents.date ?? Date() < Date() {
                        triggerComponents.year! += 1
                    }
                    if createAlarmModel.scheduleUntilDateReminders(content: content, fromDate: triggerComponents, untilDate: untilDate , unit: unit, id: notificationId, notificationDay: notificationModel.notificationDay ?? 1, notificationMonth: notificationModel.notificationMonth ?? 1) {
                        notificationDictionary = createAlarmModel.setUntilDateDictionary(content: content, fromDate: triggerComponents, untilDate: untilDate , unit: unit, notificationModel: notificationModel, selectedAlarmLabel: selectedAlarmLabel ?? "Alarm", soundName: soundName ?? "", selectedDate: selectedDate)
                    } else {
                        showAlert()
                        return
                    }
                } else {
                    showAlert()
                    return
                }
            case .repeatCountRadioButton:
                let everyCount = Int(selectedDateTime) ?? 1
                if unit == .year && triggerComponents.date ?? Date() < Date() {
                    triggerComponents.year! += 1
                }
                createAlarmModel.scheduleCountableReminders(content: content, fromDate: triggerComponents, repeatCount: repeatCount ?? 1, every: everyCount, unit: unit, id: notificationId, notificationDay: notificationModel.notificationDay ?? 1, notificationMonth: notificationModel.notificationMonth ?? 1)
                notificationDictionary = createAlarmModel.setRepeatCountDictionary(content: content, fromDate: triggerComponents, repeatCount: repeatCount ?? 1, every: everyCount, unit: unit, notificationModel: notificationModel, selectedAlarmLabel: selectedAlarmLabel ?? "Alarm", soundName: soundName ?? "", selectedDate: selectedDate)
            }
            addNotificationToFirebase(notificationDictionary: notificationDictionary, notificationId: notificationId)
        }
    }
    
    func createAlarmFromDoesntEnd(selectedType: String, selectedDateTime: String) {
        var notificationDictionary: NSDictionary?
        var triggerComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: Date())
        triggerComponents.calendar = Calendar.current
        var unit : Calendar.Component
        let createAlarmModel = CreateAlarm(center: UNUserNotificationCenter.current())
        let content      = UNMutableNotificationContent()
        content.body = "It is time to: " + (selectedAlarmLabel ?? "Alarm")
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "fizzbuzz"]
        content.sound    = UNNotificationSound(named: UNNotificationSoundName(rawValue: (soundName ?? "") + ".mp3"))
        
        unit = createAlarmModel.unitType(selectedType: selectedType)
        let notificationId = UUID().uuidString
        createAlarmModel.scheduleDoesntEndReminders(content: content, fromDate: selectedDate ?? "", unit: unit, id: notificationId, selectedNotificationDay:  Date().get(.day), notificationDate: Date())
        notificationDictionary = createAlarmModel.setDoesntEndDictionary(content: content, fromDate: triggerComponents, unit: unit, notificationDate: Date(), selectedAlarmLabel: selectedAlarmLabel ?? "Alarm", soundName: soundName ?? "", selectedDate: selectedDate, id: notificationId, alarmSubtitle: "", notificationDay:  Date().get(.day))
        addNotificationToFirebase(notificationDictionary: notificationDictionary, notificationId: notificationId)
    }
    
    private func addNotificationToFirebase(notificationDictionary: NSDictionary?, notificationId: String) {
        if let notificationDictionary = notificationDictionary {
            alarms?.add(notificationDictionary)
            UserDefaults.standard.set(true, forKey: notificationId)
            alarmService.addNotificationToFirebase(userId: userId, alarms: alarms ?? [])
            alarmViewDelegate?.notificationsAdded()
        }
    }
    
    func registerCategories(vc: AlarmViewController) {
//        center.delegate = vc
//
//        let show = UNNotificationAction(identifier: "show", title: "DONE", options: .foreground)
//        let show2 = UNNotificationAction(identifier: "show", title: "UNDONE", options: .foreground)
//        let category = UNNotificationCategory(identifier: "alarm", actions: [show, show2], intentIdentifiers: [])
//
//        center.setNotificationCategories([category])
    }
    
    func setSelectedTimes(pickerDate: Date, notificationId: Int) {
        checkIds(notificationId: notificationId)
        let dictionary: NSDictionary = [
            "id" : UUID().uuidString,
            "title" : (selectedAlarmLabel ?? "Alarm"),
            "notificationDate" : pickerDate
        ]
        notificationModels.add(dictionary)
    }
    
    func setSelectedWeekDayTimes(selectedHour: String, pickerDate: Date, notificationId: Int) {
        if selectedWeekDays.count == 0 {
            return
        }
        checkIds(notificationId: notificationId)
        selectedWeekDays = selectedWeekDays.map {
            var mutableAlarm = $0
            if mutableAlarm.id == notificationId {
                mutableAlarm.selectedHour = selectedHour
                mutableAlarm.pickerDate = pickerDate
                self.addWeekModelToArray(model: mutableAlarm)
            }
            return mutableAlarm
        }
    }
    
    func setSelectedWeekDays(weekDay: String, notificationId: Int) {
        checkIds(notificationId: notificationId)
        selectedWeekDays = selectedWeekDays.map {
            var mutableAlarm = $0
            if mutableAlarm.id == notificationId {
                mutableAlarm.selectedDay = weekDay
            }
            return mutableAlarm
        }
    }
    
    private func setSelectedMonthDays(monthDay: String, notificationId: Int, selectedMonthDayRow: Int) {
        checkIds(notificationId: notificationId)
        selectedMonthDays = selectedMonthDays.map {
            var mutableAlarm = $0
            if mutableAlarm.id == notificationId {
                mutableAlarm.selectedDay = monthDay
                mutableAlarm.selectedMonthDayRow = selectedMonthDayRow
            }
            return mutableAlarm
        }
    }
    
    private func setSelectedMonthDayTimes(selectedHour: String, pickerDate: Date, notificationId: Int) {
        if selectedMonthDays.count == 0 {
            return
        }
        checkIds(notificationId: notificationId)
        selectedMonthDays = selectedMonthDays.map {
            var mutableAlarm = $0
            if mutableAlarm.id == notificationId {
                mutableAlarm.selectedHour = selectedHour
                mutableAlarm.pickerDate = pickerDate
                self.addModelToArray(monthModel: mutableAlarm)
            }
            return mutableAlarm
        }
    }
    
    private func addModelToArray(monthModel: AlarmMonthModel) {
        let dictionary: NSDictionary = [
            "id" : UUID().uuidString,
            "title" : (selectedAlarmLabel ?? "Alarm"),
            "notificationDate" : monthModel.pickerDate ?? Date(),
            "notificationDay" : monthModel.selectedMonthDayRow ?? "",
            "alarmSubtitle" : monthModel.selectedDay ?? ""
        ]
        notificationMonthModels.add(dictionary)
    }
    
    private func addWeekModelToArray(model: AlarmWeekModel) {
        let dictionary: NSDictionary = [
            "id" : UUID().uuidString,
            "title" : (selectedAlarmLabel ?? "Alarm"),
            "notificationDate" : model.pickerDate ?? Date(),
            "notificationDay" : getWeekDay(from: model.selectedDay ?? ""),
            "alarmSubtitle" : model.selectedDay ?? ""
        ]
        notificationWeekModels.add(dictionary)
    }
    
    private func setSelectedYearDay(selectedDay: Int, notificationId: Int) {
        checkIds(notificationId: notificationId)
        selectedYearDayAndTimes = selectedYearDayAndTimes.map {
            var mutableAlarm = $0
            if mutableAlarm.id == notificationId {
                mutableAlarm.selectedDay = selectedDay
            }
            return mutableAlarm
        }
    }
    
    private func setSelectedYearMonth(selectedMonth: Int, pickerDate: Date, notificationId: Int) {
        if selectedYearDayAndTimes.count == 0 {
            return
        }
        checkIds(notificationId: notificationId)
        selectedYearDayAndTimes = selectedYearDayAndTimes.map {
            var mutableAlarm = $0
            if mutableAlarm.id == notificationId {
                mutableAlarm.selectedMonth = selectedMonth
            }
            return mutableAlarm
        }
    }
    
    private func setSelectedYearTime(selectedTime: String, pickerDate: Date, notificationId: Int) {
        if selectedYearDayAndTimes.count == 0 {
            return
        }
        checkIds(notificationId: notificationId)
        selectedYearDayAndTimes = selectedYearDayAndTimes.map {
            var mutableAlarm = $0
            if mutableAlarm.id == notificationId {
                mutableAlarm.selectedTime = selectedTime
                mutableAlarm.pickerDate = pickerDate
                self.addYearModelToArray(yearModel: mutableAlarm)
            }
            return mutableAlarm
        }
    }
    
    private func addYearModelToArray(yearModel: AlarmYearModel) {
        let dictionary: NSDictionary = [
            "id" : UUID().uuidString,
            "title" : (selectedAlarmLabel ?? "Alarm"),
            "notificationDate" : yearModel.pickerDate ?? Date(),
            "notificationDay" : yearModel.selectedDay ?? "",
            "notificationMonth" : yearModel.selectedMonth ?? "",
            "notificationTime" : yearModel.selectedTime ?? "",
            "alarmSubtitle" : yearModel.selectedDay ?? ""//TODO: Değiştirilmesi lazım
        ]
        notificationYearModels.add(dictionary)
    }
    
    private func checkIds(notificationId: Int) {
        if notificationIds.count == 0 {
            notificationIds.append(notificationId)
        } else {
            for id in notificationIds {
                if id == notificationId {
                    notificationIds.remove(at: id)
                }
            }
            notificationIds.append(notificationId)
        }
    }
    
    private func getWeekDay(from weekDay: String) -> Int {
        switch weekDay {
        case "Sunday":
            return 1
        case "Monday":
            return 2
        case "Tuesday":
            return 3
        case "Wednesday":
            return 4
        case "Thursday":
            return 5
        case "Friday":
            return 6
        case "Saturday":
            return 7
        default:
            return 1
        }
    }
}

extension AlarmPresenter: DayTimeDelegate {
    
    func choosenTime(selectedHour: String, pickerDate: Date, notificationId: Int) {
        setSelectedTimes(pickerDate: pickerDate, notificationId: notificationId)
        let alarmDayModel = AlarmDayModel(selectedHour: selectedHour)
        
        if notificationId == 0 {
            selectedDayTimes = []
            for count in 0 ..< (alarmCount ?? 1) {
                selectedDayTimes.append(AlarmDayModel(id: count))
            }
            self.selectedFirstDayDate = pickerDate
        }
        
        selectedDayTimes = selectedDayTimes.map {
            var mutableAlarm = $0
            if mutableAlarm.id == notificationId {
                mutableAlarm.selectedHour = alarmDayModel.selectedHour
            }
            return mutableAlarm
        }
        alarmViewDelegate?.reloadTableView()
    }
    
    func choosenWeekDayTime(selectedHour: String, pickerDate: Date, notificationId: Int) {
        setSelectedWeekDayTimes(selectedHour: selectedHour, pickerDate: pickerDate, notificationId: notificationId)
        alarmViewDelegate?.reloadTableView()
    }
    
    func choosenWeekDay(day: String, notificationId: Int) {
        setSelectedWeekDays(weekDay: day, notificationId: notificationId)
        alarmViewDelegate?.reloadTableView()
    }
}

extension AlarmPresenter: MonthDayTimeDelegate {
    
    func choosenMonthDayTime(selectedHour: String, pickerDate: Date, notificationId: Int) {
        setSelectedMonthDayTimes(selectedHour: selectedHour, pickerDate: pickerDate, notificationId: notificationId)
        alarmViewDelegate?.reloadTableView()
    }
    
    func choosenMonthDay(day: String, notificationId: Int, selectedMonthDayRow: Int) {
        setSelectedMonthDays(monthDay: day, notificationId: notificationId, selectedMonthDayRow: selectedMonthDayRow)
        alarmViewDelegate?.reloadTableView()
    }
}

extension AlarmPresenter: YearDelegate {
    
    func choosenYearDay(selectedDay: Int, notificationId: Int) {
        setSelectedYearDay(selectedDay: selectedDay, notificationId: notificationId)
        alarmViewDelegate?.reloadTableView()
    }
    
    func choosenYearMonth(selectedMonth: Int, pickerDate: Date, notificationId: Int) {
        setSelectedYearMonth(selectedMonth: selectedMonth, pickerDate: pickerDate, notificationId: notificationId)
        alarmViewDelegate?.reloadTableView()
    }
    
    func choosenYearTime(selectedTime: String, pickerDate: Date, notificationId: Int) {
        setSelectedYearTime(selectedTime: selectedTime, pickerDate: pickerDate, notificationId: notificationId)
        alarmViewDelegate?.reloadTableView()
    }
}
