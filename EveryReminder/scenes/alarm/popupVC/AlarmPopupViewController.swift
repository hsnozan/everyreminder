//
//  AlarmPopupViewController.swift
//  EveryReminder
//
//  Created by deneme on 22.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

class AlarmPopupViewController: UIViewController {

    @IBOutlet weak var datePickerContainer: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    weak var alarmViewDelegate : AlarmViewDelagate?
    var indexPath: IndexPath?
    var selectedDate: String?
    var selectedHour: String?
    var pickerDate: Date?
    var selectedType: String?
    var selectedFirstCellDate: Date?
    var isFromRadioBtn: Bool?
    var isSelectedTypeYearAndMonth: Bool?
    var isFromChooseDateRadioBtn: Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerContainer.layer.cornerRadius = 10
        datePickerContainer.layer.masksToBounds = true
        datePicker.minimumDate = Date()
        
        if isSelectedTypeYearAndMonth ?? false {
            datePicker.datePickerMode = .date
        }
        
        if !(isFromRadioBtn ?? true) {
            checkSelectedType()
        }
    }
    
    func checkSelectedType() {
        var unit : Calendar.Component
        switch selectedType {
        case "Minute":
            unit = .minute
        case "Hour":
            unit = .hour
        case "Day":
            unit = .day
        case "Week":
            unit = .weekday
        case "Month":
            unit = .month
        case "Year":
            unit = .year
        default:
            unit = .minute
        }
        
        if let cellDate = selectedFirstCellDate, indexPath?.row != 0 {
            datePicker.minimumDate = cellDate
            if unit == .weekday {
                datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 7, to: cellDate)
            } else {
                datePicker.maximumDate = Calendar.current.date(byAdding: unit, value: 1, to: cellDate)
            }
            
        }
    }
    
    @IBAction func chooseButtonTapped(_ sender: Any) {
        if pickerDate == nil {
            formatDates(by: Date())
        }
        
        if isFromRadioBtn ?? false {
            alarmViewDelegate?.untilThisDate(pickerDate: pickerDate ?? Date())
        } else if isFromChooseDateRadioBtn ?? false {
            alarmViewDelegate?.chooseDate(pickerDate: pickerDate ?? Date())
        } else {
//            alarmViewDelegate?.choosenSpesificTime(selectedDate: selectedDate ?? "", selectedHour: selectedHour ?? "",pickerDate: pickerDate ?? Date(), notificationId: indexPath?.row ?? 0)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func datePickerChanged(_ sender: Any) {
        formatDates(by: datePicker.date)
    }
    
    @IBAction func dismissButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func formatDates(by date: Date) {
        let dateFormatterDay = DateFormatter()
        dateFormatterDay.dateFormat = "EEE-MMM-dd"
        
        let dateFormatterHour = DateFormatter()
        dateFormatterHour.dateFormat = "HH:mm"
        
        selectedDate = dateFormatterDay.string(from: datePicker.date)
        selectedHour = dateFormatterHour.string(from: datePicker.date)
        self.pickerDate = datePicker.date
    }
}
