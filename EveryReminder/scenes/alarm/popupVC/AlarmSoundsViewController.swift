//
//  ChooseAlarmViewController.swift
//  EveryReminder
//
//  Created by deneme on 4.11.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit
import AVFoundation

class AlarmSoundsViewController: UIViewController {
    
    @IBOutlet weak var soundsTableView: UITableView!
    @IBOutlet weak var soundsContainer: UIView!
    
    
    weak var alarmChooseDelegate : AlarmChooseDelagate?
    var sounds = [String]()
    var soundNames = [String]()
    var soundLabel: String?
    var soundName: String?
    var player: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        soundsContainer.layer.cornerRadius = 10
        soundsContainer.layer.masksToBounds = true

        sounds = ["Long Alarm", "Alarm", "Short Alarm", "Vibrate"]
        soundNames = ["notification", "notification_ring", "iphone_notification", "Vibrate"]
    }
    
//    override func viewDidLayoutSubviews(){
//        soundsTableView.frame = CGRect(x: soundsTableView.frame.origin.x, y: soundsTableView.frame.origin.y, width: soundsTableView.frame.size.width, height: soundsTableView.contentSize.height)
//        soundsTableView.reloadData()
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        soundsTableView.frame = CGRect(x: soundsTableView.frame.origin.x, y: soundsTableView.frame.origin.y, width: soundsTableView.frame.size.width, height: soundsTableView.contentSize.height)
//    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            guard let player = player else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    @IBAction func dismissButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chooseBtnTapped(_ sender: Any) {
        alarmChooseDelegate?.choosenSound(soundLabel: soundLabel, soundName: soundName)
        dismiss(animated: true, completion: nil)
    }
}

extension AlarmSoundsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sounds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "alarmSoundTableViewCell", for: indexPath) as? AlarmSoundTableViewCell
       
        cell?.soundLabel.text = sounds[indexPath.row]
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        soundName = soundNames[indexPath.row]
        soundLabel = sounds[indexPath.row]
        self.playSound()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}
