//
//  AlarmTableViewCell.swift
//  EveryReminder
//
//  Created by deneme on 21.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

class AlarmTableViewCell: UITableViewCell {

    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var arrowButton: UIImageView!
    @IBOutlet weak var chosenDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
