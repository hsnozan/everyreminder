//
//  YearViewController.swift
//  EveryReminder
//
//  Created by hsnozan on 8.07.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import UIKit

class YearViewController: UIViewController {

    @IBOutlet weak var yearTimeDatePicker: UIDatePicker!
    @IBOutlet weak var yearDayAndMonthPicker: UIPickerView!
    
    var yearDaysAndMonthsPickerData: [[String]] = [[String]]()
    var yearDaysOrMonths = [String]()
    weak var alarmYearDelegate: YearDelegate?
    var isFromMonthBtn : Bool? = false
    var isFromYearTimeBtn : Bool? = false
    var selectedMonth: String?
    var selectedDay: String?
    var selectedHour: String?
    var pickerDate: Date?
    var indexPath: Int?
    var selectedRow: Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideOrVisiblePicker(isFromYearTimeBtn: isFromYearTimeBtn ?? false)
        if !(isFromYearTimeBtn ?? false) {
            yearDayAndMonthPicker.delegate = self
            yearDayAndMonthPicker.dataSource = self
            yearDaysOrMonths = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
            if !isFromYearMonthBtn() {
                yearDaysOrMonths.append(contentsOf: ["13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"])
            }
            
            yearDaysAndMonthsPickerData = [yearDaysOrMonths]
        }
    }
    private func isFromYearMonthBtn() -> Bool {
        return isFromMonthBtn ?? false
    }
    
    private func hideOrVisiblePicker(isFromYearTimeBtn: Bool) {
        yearDayAndMonthPicker.isHidden = isFromYearTimeBtn
        yearTimeDatePicker.isHidden = !isFromYearTimeBtn
    }
    
    private func formatDates(by date: Date) {
        let dateFormatterHour = DateFormatter()
        dateFormatterHour.dateFormat = "HH:mm"
        
        self.pickerDate = yearTimeDatePicker.date
        selectedHour = dateFormatterHour.string(from: yearTimeDatePicker.date)
    }

    @IBAction func ChooseBtnTapped(_ sender: Any) {
        if isFromYearTimeBtn ?? false {
            if pickerDate == nil {
                formatDates(by: Date())
            }
            alarmYearDelegate?.choosenYearTime(selectedTime: selectedHour ?? "", pickerDate: pickerDate ?? Date(), notificationId: indexPath ?? 0)
        } else if isFromYearMonthBtn() {
            alarmYearDelegate?.choosenYearMonth(selectedMonth: (selectedRow ?? 0) + 1, pickerDate: pickerDate ?? Date(), notificationId: indexPath ?? 0)
        } else {
            alarmYearDelegate?.choosenYearDay(selectedDay: (selectedRow ?? 0) + 1, notificationId: indexPath ?? 0)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension YearViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearDaysOrMonths.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as? UILabel;
        if (pickerLabel == nil) {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Montserrat", size: 12)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        pickerLabel?.text = yearDaysAndMonthsPickerData[component][row]
        return pickerLabel!;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if isFromYearMonthBtn() {
            selectedMonth = yearDaysAndMonthsPickerData[component][row]
        } else {
            selectedDay = yearDaysAndMonthsPickerData[component][row]
        }
        
        selectedRow = row
    }
    
}
