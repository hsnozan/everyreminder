//
//  YearTableViewCell.swift
//  EveryReminder
//
//  Created by deneme on 30.11.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

class YearTableViewCell: UITableViewCell {

    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var dayButton: UIButton!
    @IBOutlet weak var monthButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    
    var yearDayAction: (() -> Void)?
    var yearMonthAction: (() -> Void)?
    var yearTimeAction: (() -> Void)?
    
    var alarmYearModel: AlarmYearModel? {
        didSet {
            setUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setUI() {
        if let selectedDay = alarmYearModel?.selectedDay {
            dayButton.setTitle("\(selectedDay).", for: .normal)
            dayButton.setTitleColor(.black, for: .normal)
        } else {
            dayButton.setTitle("X.", for: .normal)
            dayButton.setTitleColor(.lightGray, for: .normal)
        }
        
        if let selectedMonth = alarmYearModel?.selectedMonth {
            monthButton.setTitle("\(selectedMonth).", for: .normal)
            monthButton.setTitleColor(.black, for: .normal)
        } else {
            monthButton.setTitle("Y.", for: .normal)
            monthButton.setTitleColor(.lightGray, for: .normal)
        }
        
        if let selectedYearTime = alarmYearModel?.selectedTime {
            timeButton.setTitle(selectedYearTime, for: .normal)
            timeButton.setTitleColor(.black, for: .normal)
        } else {
            timeButton.setTitle("Time", for: .normal)
            timeButton.setTitleColor(.lightGray, for: .normal)
        }
    }
    
    @IBAction func yearDayBtnTapped(_ sender: Any) {
        yearDayAction?()
    }
    
    @IBAction func yearMonthBtnTapped(_ sender: Any) {
        yearMonthAction?()
    }
    
    @IBAction func yearTimeBtnTapped(_ sender: Any) {
        yearTimeAction?()
    }
}
