//
//  HomeViewController.swift
//  EveryReminder
//
//  Created by deneme on 10.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit
import EventKit

protocol HomeViewDelegate: AnyObject {
    func setUI(user: UserModel)
}

class HomeViewController: UIViewController, HomeViewDelegate {
    
    @IBOutlet weak var homeTableView: UITableView!
    
    private let homePresenter = HomePresenter(homeService: HomeService())
    weak var homeViewDelegate: HomeViewDelegate?
    var alarms: NSMutableArray?
    var userId: String?
    let userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeTableView.delegate = self
        homeTableView.dataSource = self
        homeViewDelegate = self
        homePresenter.setViewDelegate(homeViewDelegate: self.homeViewDelegate)
        
        let userCreated = userDefaults.bool(forKey: "userCreated")
        if !userCreated {
            homePresenter.createUser()
        } else {
            homePresenter.getAlarmsFromUsers()
        }
    }
    
    func setUI(user: UserModel) {
        self.alarms = user.alarms
        self.userId = user.userId
        homeTableView.reloadData()
    }
    
    func requestAccessToCalendar() {
        EKEventStore().requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                self.userDefaults.set(true, forKey: "CalendarGranted")
            } else {
                self.userDefaults.set(false, forKey: "CalendarGranted")
            }
        })
    }
    
    @IBAction func infoButtonClicked(_ sender: Any) {
        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { response in
            print(response)
        })
    }
    
    @IBAction func settingsButtonClicked(_ sender: Any) {
        //TODO: handle settingsbuttonclick event
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "toCategoryVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCategoryVC" {
            if let destVC = segue.destination as? UINavigationController,
                let categoryVC = destVC.topViewController as? CategoryViewController {
                categoryVC.userId = self.userId
                categoryVC.alarms = self.alarms
            }
        }
    }
}

extension HomeViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alarms?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeTableViewCell", for: indexPath) as! HomeTableViewCell
        let cellModel = alarms?[indexPath.row] as? NSDictionary
        let alarmModel = AlarmModel(dictionary: cellModel)
        cell.alarmTitle.text = alarmModel.title
        cell.alarmRemaining.text = alarmModel.alarmSubtitle
        cell.alarmModel = alarmModel
        cell.updateSwitchButton()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let model = alarms?[indexPath.row] as? NSDictionary else { return [] }
        let removeAction = UITableViewRowAction(style: .destructive, title: "Delete" , handler: { (action:UITableViewRowAction, indexPath: IndexPath) -> Void in
            let alarmModel = AlarmModel(dictionary: model)
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [(alarmModel.id ?? "")])
            self.alarms?.remove(model)
            self.homePresenter.deleteAlarm(alarms: self.alarms)
        })
        removeAction.backgroundColor = .red
        return [removeAction]
    }
}
