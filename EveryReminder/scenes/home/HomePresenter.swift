//
//  HomePresenter.swift
//  EveryReminder
//
//  Created by deneme on 10.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation
import FirebaseFirestore

class HomePresenter {
    
    private let homeService : HomeService
    weak private var homeViewDelegate : HomeViewDelegate?
    var db: Firestore!
    
    init(homeService : HomeService) {
        self.homeService = homeService
    }
    
    func setViewDelegate(homeViewDelegate : HomeViewDelegate?) {
        self.homeViewDelegate = homeViewDelegate
        db = Firestore.firestore()
    }
    
    func createUser() {
        homeService.createUser(db: db) { 
            self.getAlarmsFromUsers()
        }
    }
    
    func getAlarmsFromUsers() {
        homeService.getAlarms(db: db) { userModel in
            self.homeViewDelegate?.setUI(user: userModel)
        }
    }
    
    func deleteAlarm(alarms: NSMutableArray?) {
        homeService.deleteAlarm(db: db, alarms: alarms)
    }
}
