//
//  AlarmModel.swift
//  EveryReminder
//
//  Created by deneme on 16.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct AlarmModel {
    
    var contentBody: String?
    var contentCategoryIdentifier: String?
    var contentSound: String?
    var id: String?
    var notificationDateString: String?
    var repeatCount: Int?
    var title: String?
    var triggerDateString: String?
    var unit: String?
    var untilDateString: String?
    var notificationDate: Date?
    var triggerDate: Date?
    var untilDate: Date?
    var type: String?
    var selectedDate: String?
    var day: Int?
    var alarmSubtitle: String?
    var notificationDay: Int?
    var notificationMonth: Int?
    
    init(dictionary: NSDictionary?) {
        contentBody = dictionary?["contentBody"] as? String
        contentCategoryIdentifier = dictionary?["contentCategoryIdentifier"] as? String
        contentSound = dictionary?["contentSound"] as? String
        id = dictionary?["id"] as? String
        notificationDateString = dictionary?["notificationDate"] as? String
        repeatCount = dictionary?["repeatCount"] as? Int
        title = dictionary?["title"] as? String
        triggerDateString = dictionary?["triggerDate"] as? String
        unit = dictionary?["unit"] as? String
        untilDateString = dictionary?["untilDate"] as? String
        type = dictionary?["type"] as? String
        selectedDate = dictionary?["selectedDate"] as? String
        day = dictionary?["day"] as? Int
        alarmSubtitle = dictionary?["alarmSubtitle"] as? String
        notificationDay = dictionary?["notificationDay"] as? Int
        notificationMonth = dictionary?["notificationMonth"] as? Int
        
        notificationDate = convertStringToDate(dateString: notificationDateString)
        triggerDate = convertStringToDate(dateString: triggerDateString)
        untilDate = convertStringToDate(dateString: untilDateString)
    }
    
    func convertStringToDate(dateString: String?) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.date(from: dateString ?? "") ?? Date()
    }
}
