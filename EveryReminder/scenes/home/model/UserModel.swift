//
//  UserModel.swift
//  EveryReminder
//
//  Created by deneme on 10.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation

struct UserModel {
    
    let name: String
    let alarms: NSMutableArray
    let userId: String?
    
    init?(dictionary: [String: Any], userId: String?) {
        guard let name = dictionary["userName"] as? String else { return nil }
        self.name = name
        guard let alarms = dictionary["userAlarms"] as? NSMutableArray else { return nil }
        self.alarms = alarms
        self.userId = userId
    }
}
