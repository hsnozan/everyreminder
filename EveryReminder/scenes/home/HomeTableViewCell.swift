//
//  HomeTableViewCell.swift
//  EveryReminder
//
//  Created by deneme on 10.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var alarmTitle: UILabel!
    @IBOutlet weak var alarmRepeatTime: UILabel!
    @IBOutlet weak var alarmRemaining: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var containerView: UIView!
    
    var alarmModel: AlarmModel?
    var createAlarm: CreateAlarm?
    let userDefaults = UserDefaults.standard
    var isFirstTimeIsOn: Bool? = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateView()
        createAlarm = CreateAlarm(center: UNUserNotificationCenter.current())
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        switchButton.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
    }
    
    func updateView() {
        isFirstTimeIsOn = true
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        containerView.layer.borderColor = UIColor.gray.cgColor
        containerView.layer.borderWidth = 0.5
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
    }
    
    func updateSwitchButton() {
        if userDefaults.bool(forKey: alarmModel?.id ?? "") {
            switchButton.setOn(true, animated: false)
        } else {
            switchButton.setOn(false, animated: false)
        }
    }
    
    func createAlarmContent() -> UNMutableNotificationContent {
        let content = UNMutableNotificationContent()
        content.body = alarmModel?.contentBody ?? "Alarm"
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "fizzbuzz"]
        content.sound    = UNNotificationSound(named: UNNotificationSoundName(rawValue: (alarmModel?.contentSound ?? "") + ".mp3"))
        return content
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0))
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let isOn = mySwitch.isOn
        userDefaults.set(isOn, forKey: alarmModel?.id ?? "")
        removeOrAddNotification(isSwitchOn: isOn)
    }
    
    func removeOrAddNotification(isSwitchOn: Bool) {
        switch alarmModel?.type {
        case "DoesntEnd":
            createDoesntEnd(isSwitchOn: isSwitchOn)
        default:
            createUntilDate(isSwitchOn: isSwitchOn)
        }
    }
    
    func createDoesntEnd(isSwitchOn: Bool) {
        guard let unit = (createAlarm?.unitType(selectedType: alarmModel?.unit ?? "")) else { return }
        if !isSwitchOn {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [(self.alarmModel?.id ?? "")])
        } else {
            self.createAlarm?.scheduleDoesntEndReminders(content: createAlarmContent(), fromDate: alarmModel?.selectedDate ?? "", unit: unit, id: self.alarmModel?.id ?? "", selectedNotificationDay: alarmModel?.notificationDay ?? Date().get(.day), notificationDate: Date())
        }
    }
    
    func createUntilDate(isSwitchOn: Bool) {
        var triggerComponents: DateComponents?
        guard let unit = (createAlarm?.unitType(selectedType: alarmModel?.unit ?? "")) else { return }
        guard let alarmModelRepeatCount = alarmModel?.repeatCount else { return }

        if Date() >= (alarmModel?.triggerDate ?? Date()) {
            triggerComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: Date())
        } else {
            triggerComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: alarmModel?.triggerDate ?? Date())
        }
        triggerComponents?.calendar = Calendar.current
        
        guard let triggerDate = triggerComponents?.date else { return }
        guard let repeatCount = createAlarm?.getRepeatCount(triggerDate: triggerDate, untilDate: alarmModel?.untilDate ?? Date(), unit: unit) else { return }
        let remainingRepeatCount = alarmModelRepeatCount - repeatCount
        
        if !isSwitchOn {
            for count in remainingRepeatCount ..< alarmModelRepeatCount {
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [(self.alarmModel?.id ?? "") + String(count)])
            }
        } else {
            if let triggerComponents = triggerComponents {
                if (createAlarm?.scheduleUntilDateReminders(content: createAlarmContent(), fromDate: triggerComponents, untilDate: alarmModel?.untilDate ?? Date(), unit: createAlarm?.unitType(selectedType: alarmModel?.unit ?? "Year") ?? .year, id: alarmModel?.id ?? "", notificationDay: alarmModel?.day ?? 1, notificationMonth: alarmModel?.notificationMonth ?? 1)) ?? false {
                    print("Alarm created")
                } else {
                    // showAlert
                }
            }
        }
    }
}
