//
//  HomeService.swift
//  EveryReminder
//
//  Created by deneme on 10.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation
import FirebaseFirestore

class HomeService {
    
    var ref: DocumentReference? = nil
    let userDefaults = UserDefaults.standard
    
    func createUser(db: Firestore, callback: @escaping () -> Void) {
        ref = db.collection("users").addDocument(data: [
            "userName": "user",
            "userAlarms": []
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                self.userDefaults.set(self.ref!.documentID, forKey: "userDocID")
                self.userDefaults.set(true, forKey: "userCreated")
                callback()
            }
        }
    }
    
    func getAlarms(db: Firestore, callback: @escaping (UserModel) -> Void) {
        if let userDocID = userDefaults.string(forKey: "userDocID") {
            db.collection("users").document(userDocID).addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else { return }
                guard let dictionaryData = documentSnapshot?.data() else { return }
                if let userModel = UserModel(dictionary: dictionaryData, userId: document.documentID) {
                    callback(userModel)
                }
            }
        }
    }
    
    func deleteAlarm(db: Firestore, alarms: NSMutableArray?) {
        guard let userDocID = userDefaults.string(forKey: "userDocID") else { return }
        let sfReference = db.collection("users").document(userDocID)
        
        db.runTransaction({ (transaction, errorPointer) -> Any? in
            let sfDocument: DocumentSnapshot
            do {
                try sfDocument = transaction.getDocument(sfReference)
            } catch let fetchError as NSError {
                errorPointer?.pointee = fetchError
                return nil
            }
            
            guard let userAlarms = alarms else {
                let error = NSError(
                    domain: "AppErrorDomain",
                    code: -1,
                    userInfo: [
                        NSLocalizedDescriptionKey: "Unable to retrieve userAlarms from snapshot \(sfDocument)"
                    ]
                )
                errorPointer?.pointee = error
                return nil
            }

            transaction.updateData(["userAlarms": userAlarms], forDocument: sfReference)
            return nil
        }) { (object, error) in
            if let error = error {
                print("Transaction failed: \(error)")
            } else {
                print("Transaction successfully committed!")
            }
        }
    }
}
