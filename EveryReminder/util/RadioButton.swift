//
//  RadioButton.swift
//  EveryReminder
//
//  Created by deneme on 24.10.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation
import UIKit

class RadioButton: UIButton {
    var alternateButton:Array<RadioButton>?
    private var indicatorView: UIView?
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 15
        self.layer.borderWidth = 2.0
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.masksToBounds = true
        
        self.indicatorView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        self.indicatorView?.center = self.convert(self.center, from: self.superview)
        self.indicatorView?.layer.cornerRadius = 7.5
        self.indicatorView?.layer.masksToBounds = true
        self.addSubview(indicatorView ?? UIView())
    }
    
    func unselectAlternateButtons(){
        if alternateButton != nil {
            self.isSelected = true
            
            for aButton:RadioButton in alternateButton! {
                aButton.isSelected = false
            }
        } else {
            toggleButton()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        unselectAlternateButtons()
        super.touchesBegan(touches, with: event)
    }
    
    func toggleButton(){
        self.isSelected = !isSelected
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.indicatorView?.backgroundColor = UIColor(red: 75/255, green: 155/255, blue: 242/255, alpha: 1)
                self.layer.borderColor = UIColor(red: 75/255, green: 155/255, blue: 242/255, alpha: 1).cgColor
            } else {
                self.indicatorView?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.layer.borderColor = UIColor.gray.cgColor
            }
        }
    }
}
