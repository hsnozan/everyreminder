//
//  CreateAlarm.swift
//  EveryReminder
//
//  Created by deneme on 23.11.2019.
//  Copyright © 2019 Hasan Ozan AL. All rights reserved.
//

import Foundation
import UserNotifications
import FirebaseFirestore
import EventKit

class CreateAlarm {
    
    var center: UNUserNotificationCenter?
    
    init(center: UNUserNotificationCenter) {
        self.center = center
    }
}

extension CreateAlarm {
    
    func setDoesntEndDictionary(content: UNNotificationContent, fromDate dateComponents: DateComponents, unit: Calendar.Component = .minute, notificationDate: Date, selectedAlarmLabel: String, soundName: String, selectedDate: String?, id: String, alarmSubtitle: String, notificationDay: Int) -> NSDictionary {
        let triggerDate = dateComponents.date
        let unitString = unitStringType(unit: unit)
        let notificationDateString = convertDateToString(date: notificationDate)
        let triggerDateString = convertDateToString(date: triggerDate)
        let alarmSubtitle = createSubtitleString(selectedDate: selectedDate, alarmSubtitle: alarmSubtitle, unit: unit, notificationDay: notificationDay)
        
        let dictionary: NSDictionary = [
            "id" : id,
            "title" : (selectedAlarmLabel ),
            "notificationDate" : notificationDateString,
            "contentBody" : content.body,
            "contentCategoryIdentifier" : "alarm",
            "contentSound" : soundName ,
            "triggerDate" : triggerDateString,
            "unit" : unitString,
            "type" : "DoesntEnd",
            "selectedDate" : selectedDate ?? "",
            "alarmSubtitle" : alarmSubtitle,
            "notificationDay" : notificationDay,
            "notificationMonth" : Date().getMonthFromDate()
        ]
        
        return dictionary
    }
    
    func setUntilDateDictionary(content: UNNotificationContent, fromDate dateComponents: DateComponents, untilDate: Date, unit: Calendar.Component = .minute, notificationModel: NotificationModel, selectedAlarmLabel: String, soundName: String, selectedDate: String?) -> NSDictionary {
        var repeatCount: Int
        let triggerDate = dateComponents.date
        let unitString = unitStringType(unit: unit)
        let notificationDateString = convertDateToString(date: notificationModel.notificationDate)
        let triggerDateString = convertDateToString(date: triggerDate)
        let untilDateString = convertDateToString(date: untilDate)
        let alarmSubtitle = createSubtitleString(selectedDate: selectedDate, alarmSubtitle: notificationModel.alarmSubtitle, unit: unit, notificationDay: notificationModel.notificationDay ?? 1)
        repeatCount = getRepeatCount(triggerDate: triggerDate ?? Date(), untilDate: untilDate, unit: unit)
        let dictionary: NSDictionary = [
            "id" : notificationModel.id ?? UUID().uuidString,
            "title" : (selectedAlarmLabel ),
            "notificationDate" : notificationDateString,
            "contentBody" : content.body,
            "contentCategoryIdentifier" : "alarm",
            "contentSound" : soundName ,
            "repeatCount" : repeatCount,
            "triggerDate" : triggerDateString,
            "untilDate" : untilDateString,
            "unit" : unitString,
            "type" : "UntilDate",
            "day": notificationModel.notificationDay ?? "",
            "alarmSubtitle" : alarmSubtitle,
            "notificationDay" : notificationModel.notificationDay ?? Date().get(.day),
            "notificationMonth" : notificationModel.notificationMonth ?? 1
        ]
        
        return dictionary
    }
    
    func setRepeatCountDictionary(content: UNNotificationContent, fromDate dateComponents: DateComponents,repeatCount: Int, every: Int = 1, unit: Calendar.Component = .minute, notificationModel: NotificationModel, selectedAlarmLabel: String, soundName: String, selectedDate: String?) -> NSDictionary {
        let calendar = Calendar.current
        let triggerDate = dateComponents.date
        let unitString = unitStringType(unit: unit)
        let notificationDateString = convertDateToString(date: notificationModel.notificationDate)
        let triggerDateString = convertDateToString(date: triggerDate)
        let untilDate = calendar.date(byAdding: unit, value: every * repeatCount, to: triggerDate ?? Date())
        let untilDateString = convertDateToString(date: untilDate)
        let alarmSubtitle = createSubtitleString(selectedDate: selectedDate, alarmSubtitle: notificationModel.alarmSubtitle, unit: unit, notificationDay: notificationModel.notificationDay ?? 1)
        let dictionary: NSDictionary = [
            "id" : notificationModel.id ?? UUID().uuidString,
            "title" : (selectedAlarmLabel ),
            "notificationDate" : notificationDateString,
            "contentBody" : content.body,
            "contentCategoryIdentifier" : "alarm",
            "contentSound" : soundName ,
            "repeatCount" : repeatCount,
            "triggerDate" : triggerDateString,
            "untilDate" : untilDateString,
            "unit" : unitString,
            "type" : "RepeatCount",
            "day": notificationModel.notificationDay ?? "",
            "alarmSubtitle" : alarmSubtitle,
            "notificationDay" : notificationModel.notificationDay ?? Date().get(.day),
            "notificationDay" : notificationModel.notificationMonth ?? 1
        ]
        
        return dictionary
    }
    
    func scheduleUntilDateReminders(content: UNNotificationContent, fromDate dateComponents: DateComponents, untilDate: Date, unit: Calendar.Component = .minute, id: String, notificationDay: Int, notificationMonth: Int) -> Bool {
        let calendar = Calendar.current
        var repeatCount: Int
        guard let triggerDate = dateComponents.date else { return false }
        
        repeatCount = getRepeatCount(triggerDate: triggerDate, untilDate: untilDate, unit: unit)
        
        if triggerDate >= Date() && triggerDate <= untilDate && repeatCount == 0 {
            repeatCount = 1
        } else if repeatCount == 0 {
            return false
        }
        for count in 0 ..< repeatCount {
            var date: Date?
            date = calendar.date(byAdding: unit, value: count, to: triggerDate)
            let reminderDateComponents = createTriggerComponent(with: unit, date: date ?? Date(), notificationDay: notificationDay, notificationMonth: notificationMonth)
            let reminderTrigger = UNCalendarNotificationTrigger(dateMatching: reminderDateComponents, repeats: false)
            let uniqueID = (id + String(count))
            addNotification(content: content, trigger: reminderTrigger , indentifier: uniqueID)
        }
        
        return true
    }
    
    func scheduleDoesntEndReminders(content: UNNotificationContent, fromDate selectedDate: String, unit: Calendar.Component = .minute, id: String, selectedNotificationDay: Int, notificationDate: Date) {
        var trigger: UNNotificationTrigger?
        if unit == .month {
            let components = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: notificationDate)
            trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
        } else if unit == .year {
            let components = Calendar.current.dateComponents([.month, .day, .hour, .minute, .second], from: notificationDate)
            trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
        } else {
            var timeInterval = TimeInterval()
            timeInterval = oneTimeAlarm(selectedDate: Int(selectedDate)!, unit: unit)
            trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval ,repeats: true)
        }
        
        addNotification(content: content, trigger: trigger, indentifier: id)
    }
    
    func scheduleCountableReminders(content: UNNotificationContent, fromDate dateComponents: DateComponents, repeatCount: Int, every: Int = 1, unit: Calendar.Component = .minute, id: String, notificationDay: Int, notificationMonth: Int) {
        let calendar = Calendar.current
        guard let triggerDate = dateComponents.date else { fatalError() }
        for count in 0 ..< repeatCount {
            guard let date = calendar.date(byAdding: unit, value: every * count, to: triggerDate) else { return }
            let reminderDateComponents =  createTriggerComponent(with: unit, date: date, notificationDay: notificationDay, notificationMonth: notificationMonth)
            let reminderTrigger = UNCalendarNotificationTrigger(dateMatching: reminderDateComponents, repeats: false)
            let uniqueID = (id + String(count))
            addNotification(content: content, trigger: reminderTrigger , indentifier: uniqueID)
        }
    }
    
    func unitType(selectedType: String) -> Calendar.Component {
        switch selectedType {
        case "Minute":
            return .minute
        case "Hour":
            return .hour
        case "Day":
            return .day
        case "Week":
            return .weekday
        case "Month":
            return .month
        case "Year":
            return .year
        default:
            return .minute
        }
    }
    
    func getRepeatCount(triggerDate: Date, untilDate: Date, unit: Calendar.Component) -> Int {
        switch unit {
        case .minute:
            return minutes(from: triggerDate, to: untilDate)
        case .hour:
            return hours(from: triggerDate, to: untilDate)
        case .day:
            return days(from: triggerDate, to: untilDate)
        case .weekday:
            return weeks(from: triggerDate, to: untilDate)
        case .month:
            return months(from: triggerDate, to: untilDate)
        case .year:
            return years(from: triggerDate, to: untilDate)
        default:
            return minutes(from: triggerDate, to: untilDate)
        }
    }
    
    // MARK: PRIVATE FUNCS
    
    private func convertDateToString(date: Date?) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: date ?? Date())
    }
    
    private func addNotification(content: UNNotificationContent, trigger: UNNotificationTrigger?, indentifier: String) {
        let request = UNNotificationRequest(identifier: indentifier, content: content, trigger: trigger)
        print(request)
        center?.add(request) { error in
            if let error = error {
                print("Error \(error.localizedDescription) in notification \(indentifier)")
            }
        }
    }
    
    private func oneTimeAlarm(selectedDate: Int, unit: Calendar.Component) -> TimeInterval {
        switch unit {
        case .minute:
            return minuteToSec(time: Int(selectedDate))
        case .hour:
            return hourToSec(time: Int(selectedDate))
        case .day:
            return dayToSec(time: Int(selectedDate))
        case .weekday:
            return weekToSec(time: Int(selectedDate))
        case .month:
            return monthToSec(time: Int(selectedDate))
        case .year:
            return yearToSec(time: Int(selectedDate))
        default:
            return hourToSec(time: Int(selectedDate))
        }
    }
    
    private func years(from date: Date, to toDate: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: toDate).year ?? 1
    }
    /// Returns the amount of months from another date
    private func months(from date: Date, to toDate: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: toDate).month ?? 1
    }
    /// Returns the amount of weeks from another date
    private func weeks(from date: Date, to toDate: Date) -> Int {
        return Calendar.current.dateComponents([.weekday], from: date, to: toDate).weekday ?? 1
    }
    /// Returns the amount of days from another date
    private func days(from date: Date, to toDate: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: toDate).day ?? 1
    }
    /// Returns the amount of hours from another date
    private func hours(from date: Date, to toDate: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: toDate).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    private func minutes(from date: Date, to toDate: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: toDate).minute ?? 0
    }
    
    private func unitStringType(unit: Calendar.Component) -> String {
        switch unit {
        case .minute:
            return "Minute"
        case .hour:
            return "Hour"
        case .day:
            return "Day"
        case .weekday:
            return "Week"
        case .month:
            return "Month"
        case .year:
            return "Year"
        default:
            return "Minute"
        }
    }
    
    private func createRepetableAlarm(selectedDate: Int, selectedType: String, selectedTime: Int) -> TimeInterval {
        switch selectedType {
        case "Minute":
            return minuteToSec(time: Int(selectedDate))
        case "Hour":
            return hourToSec(time: Int(selectedDate))
        case "Day":
            return dayToSec(time: Int(selectedDate))
        case "Week":
            return weekToSec(time: Int(selectedDate))
        case "Month":
            return monthToSec(time: Int(selectedDate))
        case "Year":
            return yearToSec(time: Int(selectedDate))
        default:
            return minuteToSec(time: Int(selectedDate))
        }
    }
    
    private func minuteToSec(time: Int?) -> Double {
        return Double((time ?? 1) * 60)
    }
    
    private func hourToSec(time: Int?) -> Double {
        return Double((time ?? 1) * 60 * 60)
    }
    
    private func dayToSec(time: Int?) -> Double {
        return Double((time ?? 1) * 24 * 360 )
    }
    
    private func weekToSec(time: Int?) -> Double {
        return Double((time ?? 1) * 24 * 360 * 7)
    }
    
    private func monthToSec(time: Int?) -> Double {
        return Double((time ?? 1) * 24 * 360 * 30)
    }
    
    private func yearToSec(time: Int?) -> Double {
        return Double((time ?? 1) * 24 * 360 * 360)
    }
    
    private func createTriggerComponent(with type: Calendar.Component, date: Date, notificationDay: Int, notificationMonth: Int) -> DateComponents {
        var triggerComponents = DateComponents()
        switch type {
        case .minute, .hour, .day:
            return Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        case .weekday:
            triggerComponents = Calendar.current.dateComponents([.month, .day ,.weekday, .hour, .minute, .second], from: date)
            triggerComponents.weekday = notificationDay
            return triggerComponents
        case .month:
            triggerComponents = Calendar.current.dateComponents([.month, .day, .hour, .minute, .second], from: date)
            return triggerComponents
        case .year:
            triggerComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
            triggerComponents.month = notificationMonth
            triggerComponents.day = notificationDay
            return triggerComponents
        default:
            return Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        }
    }
    
    private func createSubtitleString(selectedDate: String?, alarmSubtitle: String?, unit: Calendar.Component = .minute, notificationDay: Int) -> String {
        var subtitleEndString = ""
        var subtitleString = ""
        if unit == .minute {
            subtitleEndString = selectedDate == "1" ? " minute " : " minutes "
            subtitleString = "Every " + (selectedDate ?? "") + subtitleEndString
        } else if unit == .hour {
            subtitleEndString = selectedDate == "1" ? " hour " : " hours "
            subtitleString = "Every " + (selectedDate ?? "") + subtitleEndString
        } else if unit == .day {
            subtitleEndString = selectedDate == "1" ? " day " : " days "
            subtitleString = "Every " + (selectedDate ?? "") + subtitleEndString
        } else if unit == .weekday {
            subtitleEndString = selectedDate == "1" ? " week on " : " weeks on "
            subtitleString = "Every " + (selectedDate ?? "") + subtitleEndString + (alarmSubtitle ?? "")
        } else if unit == .month {
            subtitleString = "Every " + "\(notificationDay.ordinal)" + " day of the month"
        } else {
            subtitleEndString = selectedDate == "1" ? " year " : " years "
            subtitleString = "Every " + (selectedDate ?? "") + subtitleEndString
        }
        return subtitleString
    }
}

extension Int {
    var ordinal: String {
        var suffix: String
        let ones: Int = self % 10
        let tens: Int = (self/10) % 10
        if tens == 1 {
            suffix = "th"
        } else if ones == 1 {
            suffix = "st"
        } else if ones == 2 {
            suffix = "nd"
        } else if ones == 3 {
            suffix = "rd"
        } else {
            suffix = "th"
        }
        return "\(self)\(suffix)"
    }
}
