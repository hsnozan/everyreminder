//
//  Date.swift
//  EveryReminder
//
//  Created by hsnozan on 24.05.2020.
//  Copyright © 2020 Hasan Ozan AL. All rights reserved.
//

import Foundation

extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    func isMonday() -> Bool {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.weekday], from: self)
        return components.weekday == 2
    }
    
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }
    
    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
    
    static func from(year: Int, month: Int, day: Int, hour: Int, minute: Int) -> Date? {
        let calendar = Calendar(identifier: .gregorian)
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour
        dateComponents.minute = minute
        return calendar.date(from: dateComponents) ?? nil
    }
    
    func getHourFromDate() -> Int {
        let formatter = DateFormatter()
        let calendar = Calendar.current
        formatter.dateFormat = "HH"
        let hour = calendar.component(.hour, from: self)
        return hour
    }
    
    func getMinuteFromDate() -> Int {
        let formatter = DateFormatter()
        let calendar = Calendar.current
        formatter.dateFormat = "mm"
        let minute = calendar.component(.minute, from: self)
        return minute
    }
    
    func getYearFromDate() -> Int {
        let formatter = DateFormatter()
        let calendar = Calendar.current
        formatter.dateFormat = "yyyy"
        let year = calendar.component(.year, from: self)
        return year
    }
    
    func getMonthFromDate() -> Int {
        let formatter = DateFormatter()
        let calendar = Calendar.current
        formatter.dateFormat = "MM"
        let month = calendar.component(.month, from: self)
        return month
    }
    
}
